<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords"           content="" />
<meta name="Description"        content="" />
<!--Favicon shortcut link-->
<!--Declare page as mobile friendly --> 
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0"/>
<!-- Declare page as iDevice WebApp friendly -->
<meta name="apple-mobile-web-app-capable" content="yes"/>
<!-- iDevice WebApp Splash Screen, Regular Icon, iPhone, iPad, iPod Retina Icons -->
<link rel="apple-touch-icon" sizes="114x114" href="../Public/images/splash-icon.png"> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen.png" media="screen and (max-device-width: 320px)" /> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen@2x.png" media="(max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2)" /> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen@3x.png" sizes="640x1096">

<!-- Page Title -->
<title>Epsilon Framework</title>

<!-- Stylesheet Load -->
<link href="../Public/css/style.css"               rel="stylesheet"    type="text/css">
<link href="../Public/css/framework-style.css"     rel="stylesheet"    type="text/css">
<link href="../Public/css/framework.css"           rel="stylesheet"    type="text/css">
<link href="../Public/css/icons.css"               rel="stylesheet"    type="text/css">
<link href="../Public/css/retina.css"              rel="stylesheet"    type="text/css"     media="only screen and (-webkit-min-device-pixel-ratio: 2)" />

<!--Page Scripts Load -->
<script src="../Public/js/jquery.min.js"     type="text/javascript"></script>        
<script src="../Public/js/jquery-ui-min.js"  type="text/javascript"></script>
<script src="../Public/js/contact.js"        type="text/javascript"></script>
<script src="../Public/js/swipe.js"          type="text/javascript"></script>
<script src="../Public/js/klass.min.js"      type="text/javascript"></script>
<!-- <script src="../Public/js/photoswipe.js"     type="text/javascript"></script> -->
<script src="../Public/js/colorbox.js"       type="text/javascript"></script>
<script src="../Public/js/retina.js"         type="text/javascript"></script>
<script src="../Public/js/modernizr.js"      type="text/javascript"></script>
<script src="../Public/js/slicebox.js"       type="text/javascript"></script>
<script src="../Public/js/twitter.js"        type="text/javascript"></script>
<script src="../Public/js/custom.js"         type="text/javascript"></script>
</head>

<body>

<div id="preloader">
    <div id="status">
        <p class="center-text">
            <em>Loading the content...</em>
            <em>Loading depends on your connection speed!</em>
        </p>
    </div>
</div>

<div class="header-decoration"></div>

<div class="nav-page-icons content">
    <a href="<?php echo U('Index/index');?>"        id="nav0" class="go-index">回到首页</a>
    <a href="<?php echo U('ShopCart/index');?>"         id="nav1" class="go-home">查看购物车</a>
    <a href="<?php echo U('Shop/index');?>"         id="nav1" class="go-home"><b>商品分类</b></a>
    <a href="<?php echo U('Favorites/index');?>"     id="nav3" class="go-features">我的最爱</a>
    <a href="<?php echo U('Member/purchase_history');?>"      id="nav4" class="go-gallery">交易记录</a>
    <a href="<?php echo U('Member/index');?>"        id="nav2" class="go-about">用户中心</a>
    <div class="nav-decoration decoration"></div>
</div>

<div class="header">
    <a href="<?php echo U('Index/index');?>" class="deploy-share"></a>
    <h4><?php echo ($title); ?></h4>
    <a href="#" class="deploy-menu"></a>
    <div class="decoration"></div>
</div>
<div class="content">
    <div class="decoration"></div>
	<div class="container no-bottom">
        <div class="contact-form no-bottom"> 
            <div class="formSuccessMessageWrap" id="formSuccessMessageWrap">
                <div class="notification-box green-box">
                    <h4>您已经注册成功！</h4>
                    <a href="#" class="close-notification">x</a>
                    <div class="clear"></div>
                    <p>
                        感谢您的注册！<br/>
                        您现在可以关闭页面，回到威信发送消息“绑定+威信号”的形式绑定您的威信与平台。<br/>
                        使用您的威信帐号进入本平台，不再需要注册，感谢您的使用！
                    </p>
                </div>
            </div>
        
            <form action="__URL__" method="post" class="contactForm" id="contactForm">
                <fieldset>
                    <!--<span class="formValidationError" id="contactNameFieldError">-->
                        <!--<div class="small-notification red-notification">-->
                            <!--<p>请输入你的用户名</p>-->
                        <!--</div>-->
                    <!--</span>-->
                    <span class="formValidationError" id="contactEmailFieldError">
                        <div class="small-notification red-notification">
                            <p>请输入你的联系电话（手机）！</p>
                        </div>
                    </span>
                    <span class="formValidationError" id="wx_nameFieldError">
                        <div class="small-notification red-notification">
                            <p>请输入你的威信帐号！</p>
                        </div>
                    </span>
                    <span class="formValidationError" id="contactpasswordField">
                        <div class="small-notification red-notification">
                            <p>请输入你的帐号密码！</p>
                        </div>
                    </span>

                    <span class="formValidationError" id="contactNameFieldError">
                        <div class="small-notification red-notification">
                            <p>请输入你的联系姓名！</p>
                        </div>
                    </span>
                    <span class="formValidationError" id="contactMessageTextareaError">
                        <div class="small-notification red-notification">
                            <p>请输入你的常用地址</p>
                        </div>
                    </span>
                    <!--<div class="formFieldWrap">-->
                        <!--<label class="field-title contactNameField" for="contactNameField">Name:<span>(required)</span></label>-->
                        <!--<input type="text" name="contactNameField" value="" class="contactField requiredField" id="contactNameField"/>-->
                    <!--</div>-->


                    <div class="formFieldWrap">
                        <label class="field-title contactEmailField" for="contactEmailField">您的联系手机（做为登录帐号使用）: <span>(required)</span></label>
                        <input type="text" name="phone" value="" class="contactField requiredField" id="contactEmailField"/>
                    </div>

                    <div class="formFieldWrap">
                        <label class="field-title contactEmailField" for="contactpasswordField">密码: <span>(required)</span></label>
                        <input type="password" name="password" value="" class="contactField requiredField" id="contactEmailField"/>
                    </div>

                    <div class="formFieldWrap">
                        <label class="field-title contactEmailField" for="wx_nameField">威信帐号: <span>(required)</span></label>
                        <input type="text" name="wx_name" value="" class="contactField requiredField" id="wx_nameField"/>
                    </div>

                    <div class="formFieldWrap">
                        <label class="field-title contactEmailField" for="contactNameField">您的联系姓名: <span>(required)</span></label>
                        <input type="text" name="name" value="" class="contactField requiredField" id="contactNameField"/>
                    </div>
                    <div class="formTextareaWrap">
                        <label class="field-title contactMessageTextarea" for="contactMessageTextarea">您的收货地址: <span>(required)</span></label>
                        <textarea name="address" class="contactTextarea requiredField" id="contactMessageTextarea"></textarea>
                    </div>
                    <div class="formSubmitButtonErrorsWrap">
                        <input type="submit" class="buttonWrap button grey contactSubmitButton" id="contactSubmitButton" value="提交注册" data-formId="contactForm"/>
                    </div>
                </fieldset>
                <input type="hidden" name="boss_id" value="<?php echo ($boss_id); ?>"/>
            </form>       
        </div>
    </div>
	<div class="decoration"></div>
</div>
<?php if(!$noreturn):?>
<a href="javascript:history.back()" class="button-minimal grey-minimal fullscreen-button half-bottom">返回上一页</a>
<?php endif;?>
<div class="landing-footer">
    <p class="center-text footer-text">
        <br>Copyright 2013<br>konakona Design.<br>
    </p>

    <div class="footer-decoration"></div>
</div>

</body>
</html>