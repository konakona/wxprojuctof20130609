<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords"           content="" />
<meta name="Description"        content="" />
<!--Favicon shortcut link-->
<!--Declare page as mobile friendly --> 
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0"/>
<!-- Declare page as iDevice WebApp friendly -->
<meta name="apple-mobile-web-app-capable" content="yes"/>
<!-- iDevice WebApp Splash Screen, Regular Icon, iPhone, iPad, iPod Retina Icons -->
<link rel="apple-touch-icon" sizes="114x114" href="../Public/images/splash-icon.png"> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen.png" media="screen and (max-device-width: 320px)" /> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen@2x.png" media="(max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2)" /> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen@3x.png" sizes="640x1096">

<!-- Page Title -->
<title>Epsilon Framework</title>

<!-- Stylesheet Load -->
<link href="../Public/css/style.css"               rel="stylesheet"    type="text/css">
<link href="../Public/css/framework-style.css"     rel="stylesheet"    type="text/css">
<link href="../Public/css/framework.css"           rel="stylesheet"    type="text/css">
<link href="../Public/css/icons.css"               rel="stylesheet"    type="text/css">
<link href="../Public/css/retina.css"              rel="stylesheet"    type="text/css"     media="only screen and (-webkit-min-device-pixel-ratio: 2)" />

<!--Page Scripts Load -->
<script src="../Public/js/jquery.min.js"     type="text/javascript"></script>        
<script src="../Public/js/jquery-ui-min.js"  type="text/javascript"></script>
<script src="../Public/js/contact.js"        type="text/javascript"></script>
<script src="../Public/js/swipe.js"          type="text/javascript"></script>
<script src="../Public/js/klass.min.js"      type="text/javascript"></script>
<!-- <script src="../Public/js/photoswipe.js"     type="text/javascript"></script> -->
<script src="../Public/js/colorbox.js"       type="text/javascript"></script>
<script src="../Public/js/retina.js"         type="text/javascript"></script>
<script src="../Public/js/modernizr.js"      type="text/javascript"></script>
<script src="../Public/js/slicebox.js"       type="text/javascript"></script>
<script src="../Public/js/twitter.js"        type="text/javascript"></script>
<script src="../Public/js/custom.js"         type="text/javascript"></script>
</head>

<body>

<div id="preloader">
    <div id="status">
        <p class="center-text">
            <em>Loading the content...</em>
            <em>Loading depends on your connection speed!</em>
        </p>
    </div>
</div>

<div class="header-decoration"></div>

<div class="nav-page-icons content">
    <a href="<?php echo U('Index/index');?>"        id="nav0" class="go-index">回到首页</a>
    <a href="<?php echo U('ShopCart/index');?>"         id="nav1" class="go-home">查看购物车</a>
    <a href="<?php echo U('Shop/index');?>"         id="nav1" class="go-home"><b>商品分类</b></a>
    <a href="<?php echo U('Favorites/index');?>"     id="nav3" class="go-features">我的最爱</a>
    <a href="<?php echo U('Member/purchase_history');?>"      id="nav4" class="go-gallery">交易记录</a>
    <a href="<?php echo U('Member/index');?>"        id="nav2" class="go-about">用户中心</a>
    <div class="nav-decoration decoration"></div>
</div>

<div class="header">
    <a href="<?php echo U('Index/index');?>" class="deploy-share"></a>
    <h4><?php echo ($title); ?></h4>
    <a href="#" class="deploy-menu"></a>
    <div class="decoration"></div>
</div>
        <script type="text/javascript">
            $(function () {
                $('.tmpDate').hide();
                $('.way').change(function () {
                    if($(this).val() == 'now'){
                        $('.tmpDate').hide();
                    }else{
                        $('.tmpDate').show();
                    }
                    return false;
                });
            });
        </script>
<div class="content">
    <div class="container no-bottom">
        <div class="contact-form no-bottom">
            <form action="__URL__/submit" method="post" class="contactForm" id="contactForm">
                <fieldset>
                    <div class="formTextareaWrap">
                        <div class="formFieldWrap">
                            <label class="field-title contactNameField" for="contactNameField">期望送达时间:</label>
                            <!--<label><input type="radio" name="" value="1"/></label>-->
                            <select name="way" class="contactField way">
                                <option value="now">立即送货,預計15分鐘送到</option>
                                <option value="define">指定時間送到...</option>
                            </select>


                            <div class="tmpDate">

                                <label class="field-title contactNameField" for="contactNameField">日期（可选最近30天）:</label>
                                <select name="day" class="contactField">

                                    <option value="<?php echo strtotime('today');?>">今天</option>
                                    <!-- 未来30天内 -->
                                    <?php for($i=1;$i<30;$i ++ ): $time = strtotime('today')+((24*3600)*$i); ?>
                                    <option value="<?php echo $time;?>"><?php echo date('d',$time);?></option>
                                    <?php endfor;?>
                                </select>

                                <label class="field-title contactNameField" for="contactNameField">时间:</label>
                                <select name="time" class="contactField">
                                    <?php for($hour = 0 ; $hour < 24 ; $hour ++ ): ?>
                                    <?php for($min = 0 ; $min < 4;$min ++):?>
                                    <option value="<?php if($hour==0 && $min == 0 ) {echo 0 ;}else{echo $x += 900;}?>"><?php echo sprintf('%02d:%02d',$hour,($min*15));?></option>
                                    <?php endfor;?>
                                    <?php endfor;?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="formSubmitButtonErrorsWrap">
                        <input type="submit" class="buttonWrap button grey" value="确认提交订单" data-formId="contactForm"/>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>


    <div class="decoration"></div>
</div>
<?php if(!$noreturn):?>
<a href="javascript:history.back()" class="button-minimal grey-minimal fullscreen-button half-bottom">返回上一页</a>
<?php endif;?>
<div class="landing-footer">
    <p class="center-text footer-text">
        <br>Copyright 2013<br>konakona Design.<br>
    </p>

    <div class="footer-decoration"></div>
</div>

</body>
</html>