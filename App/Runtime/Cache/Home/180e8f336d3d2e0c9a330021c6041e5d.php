<?php if (!defined('THINK_PATH')) exit();?>
<!DOCTYPE HTML>
<html lang="en">
<head>
	<title>{￥title}</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<link rel="stylesheet" type="text/css" href="../Public/css/style.css" media="screen" />
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body>

	<!-- Header incl. Navigation & Search -->
	<header>
		<div class="wrap">
			<nav>
				<ul>
					<li class="active"><a href="<?php echo U('Index/index');?>" title="Home">便利店</a></li>
					<li class="dropdown collapsed">
						<a title="Catalog">网站功能</a>
						<ul>
							<li class="even"><a href="collection.htm" title="下期预告">下期预告</a></li>
							<li class="odd"><a href="collection.htm" title="其它附属优惠活动">其它附属优惠活动</a></li>
							<li class="even"><a href="collection.htm" title="身边点滴惊喜">身边点滴惊喜</a></li>
							<li class="odd"><a href="collection.htm" title="点滴惊喜">点滴惊喜</a></li>
<li class="even"><a href="collection.htm" title="新品展示">新品展示</a></li>
							<li class="odd"><a href="collection.htm" title="业务拓展">业务拓展</a></li>
						</ul>	
					</li>
					<li><a href="page.htm" title="About"><img src="../Public/images/user.png" alt="帐号管理" /></a></li>
					<li><a href="blog.htm" title="Blog"><img src="../Public/images/shop_cart.png" alt="购物车"></a></li>
				</ul>
			</nav>
			<form method="get" action="search.htm">
				<div>
					<input type="text" name="" class="search-input" value="Search.." />
					<input type="image" src="../Public/images/icon-search.png" class="search-submit" alt="Search" />
				</div>
			</form>
		</div>
	</header>
	
	<!-- Title incl. Logo & Social Media Buttons -->
	<section id="title">

		<form method="post" action="page.htm" style="height:45px;">
						<label for="form-input"></label>
						<input id="form-input" name="form-input" size="40" style="height:35px;" value="在此输入想要直接预订的商品信息...">
						<a href="page.htm" title="Button" class="button" style="">预订</a>
				</form>
		<!-- <h1><a href="/" title="Logo"><img src="../Public/images/logo.png" alt="Logo" /></a></h1> -->
		<ul style="padding-bottom:10px;">
			<li>
				<a href="" style="display:inline;"><img src="../Public/images/yuyin.png" /></a>
			</li></ul>
	</section>
	
	<section id="content">
	
		<section id="left-column">
			<!--<style>
			.temp_1{}
			.temp_1 a{color: #000; text-decoration: overline; padding-left:9px;}
			</style>
<div class="temp_1" style="padding-bottom:10px;padding-left:5px;color: #ccc;">
			<a href="我的最爱" class="grey">我的最爱</a> | <a href="交易记录">交易记录</a> | <a href="热卖促销">热卖促销</a> | <a href="我的最爱">商品分类</a>
		</div>-->

<style>
.usr-btn {
margin-bottom: 15px;
height: 36px;
line-height: 19px;
}
.usr-btn a {
border: 1px solid #b8d9be;
color: #fff;
background: black;
padding:5px;
font-size:16px;
margin-right:4px;
}
.tbf{
	/*padding:30px 15px 30px 15px ;margin-bottom:1px;font-size:14px;*/
	padding: 12px;
}
</style>
<div style="padding-bottom:15px;">
<!-- <span class="usr-btn"><a href="http://book.douban.com/guess_redirect?item_id=817920" target="_blank">我的最爱</a></span>

<span class="usr-btn"><a href="http://book.douban.com/guess_redirect?item_id=817920" target="_blank">交易记录</a></span>

<span class="usr-btn"><a href="http://book.douban.com/guess_redirect?item_id=817920" target="_blank">热卖促销</a></span>

<span class="usr-btn"><a href="http://book.douban.com/guess_redirect?item_id=817920" target="_blank">商品分类</a></span>
 -->
<a class="tbf button" href="page.htm" title="Button" >我的最爱</a>
<a class="tbf button" href="page.htm" title="Button" >交易记录</a>
<a class="tbf button" href="page.htm" title="Button" style="">热卖促销</a>
<a class="tbf button" href="page.htm" title="Button"  style="">商品分类</a>
</div>

		<section class="sale">
				<h1>常用功能</h1>
				<ul>
					<li class="slide">

						<div class="product">
							<img src="../Public/images/zuiai.png" alt="Product Image">
							<div class="overlay">
								<div class="price">
									<h2>Colored Cards</h2>
									<ul>
										<li><b>$12.95</b> <del>$14.95</del></li>
										<li class="details"><a href="product.htm" title="More Details">More Details</a></li>
									</ul>
								</div>	
							</div>
						</div>
						<div class="product">
							<img src="../Public/images/jilu.png" alt="Product Image">
							<div class="overlay">
								<div class="price">
									<h2>Rockstar Freelancer Paperback</h2>
									<ul>
										<li><b>$39.00</b> <del>$44.00</del></li>
										<li class="details"><a href="product.htm" title="More Details">More Details</a></li>
									</ul>
								</div>	
							</div>
						</div>
						<div class="product">
							<img src="../Public/images/cuxiao.png" alt="Product Image">
							<div class="overlay">
								<div class="price">
									<h2>Mini Cards</h2>
									<ul>
										<li><b>$6.95</b> <del>$9.95</del></li>
										<li class="details"><a href="product.htm" title="More Details">More Details</a></li>
									</ul>
								</div>	
							</div>
						</div>
						<div class="product">
							<img src="../Public/images/fenlei.jpg" alt="Product Image">
							<div class="overlay">
								<div class="price">
									<h2>Mini Cards</h2>
									<ul>
										<li><b>$6.95</b> <del>$9.95</del></li>
										<li class="details"><a href="product.htm" title="More Details">More Details</a></li>
									</ul>
								</div>	
							</div>
						</div>
					</li>
				</ul>
			</section>


			<!-- Product Slideshow -->
			
			
			<!-- Featured Products -->
			
			<!-- Specials — Please note that a class="last" will be required for the last item in the row -->
<!-- 			<section id="specials">
				<ul>
					<li class="first"><a href="index.htm" title="Black Friday Sale"><img src="../Public/images/special-left.jpg" alt="Product Image" /></a></li>
					<li class="second"><a href="index.htm" title="Gift Ideas"><img src="../Public/images/special-center.jpg" alt="Product Image" /></a></li>
					<li class="third"><a href="index.htm" title="Social Media"><img src="../Public/images/special-right.jpg" alt="Product Image" /></a></li>
				</ul>
			</section> -->
			
			<!-- Secondary Feature Area — Useful for Sales -->
			<!-- <section class="sale">
				<h1 style="height:10px;">
				热卖促销<a href="collection.htm" title="See All Products">查看全部</a>
			</h1>
				<ul>
					<li class="slide">

						<div class="product">我的最爱
							<img src="../Public/images/product-7.jpg" alt="我的最爱" />
							<div class="overlay">
								<div class="price">
									<h2>Colored Cards</h2>
									<ul>
										<li style="width:70px"><b>我的最爱</b> <del>我的最爱</del></li>
										<li class="details"><a href="product.htm" title="More Details">More Details</a></li>
									</ul>
								</div>	
							</div>
						</div>
						<div class="product">
							<img src="../Public/images/product-6.jpg" alt="交易记录" />
							<div class="overlay">
								<div class="price">
									<h2>Colored Cards</h2>
									<ul>
										<li style="width:70px"><b>交易记录</b> <del>交易记录</del></li>
										<li class="details"><a href="product.htm" title="More Details">More Details</a></li>
									</ul>
								</div>	
							</div>
						</div>
						<div class="product">
							<img src="../Public/images/product-8.jpg" alt="Product Image" />
							<div class="overlay">
								<div class="price">
									<h2>Colored Cards</h2>
									<ul>
										<li style="width:70px"><b>热卖促销</b> <del>热卖促销</del></li>
										<li class="details"><a href="product.htm" title="More Details">More Details</a></li>
									</ul>
								</div>	
							</div>
						</div>
						<div class="product">
							<img src="../Public/images/product-8.jpg" alt="Product Image" />
							<div class="overlay">
								<div class="price">
									<h2>Colored Cards</h2>
									<ul>
										<li style="width:70px"><b>商品分类</b> <del>商品分类</del></li>
										<li class="details"><a href="product.htm" title="More Details">More Details</a></li>
									</ul>
								</div>	
							</div>
						</div>
										
					</li>
				</ul>


			</section> -->


<section class="sale">
				<h1>秒杀活动<a href="collection.htm" title="See All Products">查看全部</a></h1>
				<ul>
					<li class="slide">

						<div class="product">
							<img src="../Public/images/product-2.jpg" alt="Product Image">
							<div class="overlay">
								<div class="price">
									<h2>Colored Cards</h2>
									<ul>
										<li><b>$12.95</b> <del>$14.95</del></li>
										<li class="details"><a href="product.htm" title="More Details">More Details</a></li>
									</ul>
								</div>	
							</div>
						</div>
						<div class="product">
							<img src="../Public/images/product-2.jpg" alt="Product Image">
							<div class="overlay">
								<div class="price">
									<h2>Rockstar Freelancer Paperback</h2>
									<ul>
										<li><b>$39.00</b> <del>$44.00</del></li>
										<li class="details"><a href="product.htm" title="More Details">More Details</a></li>
									</ul>
								</div>	
							</div>
						</div>
						<div class="product">
							<img src="../Public/images/product-8.jpg" alt="Product Image">
							<div class="overlay">
								<div class="price">
									<h2>Mini Cards</h2>
									<ul>
										<li><b>$6.95</b> <del>$9.95</del></li>
										<li class="details"><a href="product.htm" title="More Details">More Details</a></li>
									</ul>
								</div>	
							</div>
						</div>
					</li>
				</ul>
			</section>
	</section>


<section id="slideshow">				
				<div class="carousel-frame" style="width:300px;height:189px;overflow:hidden"><ul class="slider responsive" style="width: 900px; height: 189px; overflow: hidden; display: block; margin-left: -300px;">
				
					<!-- Repeatable Item Begin -->
					<li style="width: 300px; height: 189px; float: left;">
						<h1>广告区域</h1>
						<img src="../Public/images/slideshow-1.jpg" alt="Product Image">
					</li>
					<!-- Repeatable Item End -->
					
					<!-- Repeatable Item Begin -->
					<li style="width: 300px; height: 189px; float: left;">
						<h1>广告区域</h1>
						<img src="../Public/images/slideshow-2.jpg" alt="Product Image">
						<div class="price">
							<ul>
								<li>just <b>$8.99</b></li>
								<li class="details"><a href="product.htm" title="More Details">More Details</a></li>
							</ul>
						</div>
					</li>
					<!-- Repeatable Item End -->
					
					<!-- Repeatable Item Begin -->
					<li style="width: 300px; height: 189px; float: left;">
						<h1>广告区域</h1>
						<img src="../Public/images/slideshow-3.jpg" alt="Product Image">
						<div class="price">
							<ul>
								<li>just <b>$7.99</b></li>
								<li class="details"><a href="product.htm" title="More Details">More Details</a></li>
							</ul>
						</div>	
					</li>
					<!-- Repeatable Item End -->
					
				</ul></div>
				
				<!-- Slideshow Controls -->
				<a title="Previous" class="slider-prev">Previous</a>
				<a title="Next" class="slider-next">Next</a>
				
			</section>


<!-- 		<section id="title" style="padding-top: 0px;">

		<form method="post" action="page.htm" style="">
						<label for="form-input"></label>
						<input id="form-input" name="form-input" size="40" style="" value="输入商品名称...">
						<a href="page.htm" title="Button" class="button">搜索</a>
				</form>
		<ul></ul>
	</section> -->


	
	<!-- Footer incl. Copyright and Secondary Navigation -->
	<footer>
		<p><b>Copyright &copy; 2013 konakona.</b> All Rights Reserved.</p>
		<nav>
			<ul>
				<li class="active"><a href="index.htm" title="Home">首页</a></li>
				<li><a href="collection.htm" title="Catalog">商品分类</a></li>
				<li><a href="blog.htm" title="Blog">购物车</a></li>
				<li><a href="page.htm" title="About">关于公司</a></li>
			</ul>
		</nav>
	</footer>

<!-- JQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

<!-- 	
	Simple Carousel
	Copyright (c) 2010 Tobias Zeising, http://www.aditu.de
	Licensed under the MIT license
	Version 0.3
	
	http://code.google.com/p/simple-carousel
-->
<script src="../Public/js/slider.js"></script>

<!-- Script Controls -->
<script src="../Public/js/scripts.js"></script>

</body>
</html>