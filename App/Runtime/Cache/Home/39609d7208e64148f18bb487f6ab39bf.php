<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords"           content="" />
<meta name="Description"        content="" />
<!--Favicon shortcut link-->
<!--Declare page as mobile friendly --> 
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0"/>
<!-- Declare page as iDevice WebApp friendly -->
<meta name="apple-mobile-web-app-capable" content="yes"/>
<!-- iDevice WebApp Splash Screen, Regular Icon, iPhone, iPad, iPod Retina Icons -->
<link rel="apple-touch-icon" sizes="114x114" href="../Public/images/splash-icon.png"> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen.png" media="screen and (max-device-width: 320px)" /> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen@2x.png" media="(max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2)" /> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen@3x.png" sizes="640x1096">

<!-- Page Title -->
<title>Epsilon Framework</title>

<!-- Stylesheet Load -->
<link href="../Public/css/style.css"               rel="stylesheet"    type="text/css">
<link href="../Public/css/framework-style.css"     rel="stylesheet"    type="text/css">
<link href="../Public/css/framework.css"           rel="stylesheet"    type="text/css">
<link href="../Public/css/icons.css"               rel="stylesheet"    type="text/css">
<link href="../Public/css/retina.css"              rel="stylesheet"    type="text/css"     media="only screen and (-webkit-min-device-pixel-ratio: 2)" />

<!--Page Scripts Load -->
<script src="../Public/js/jquery.min.js"     type="text/javascript"></script>        
<script src="../Public/js/jquery-ui-min.js"  type="text/javascript"></script>
<script src="../Public/js/contact.js"        type="text/javascript"></script>
<script src="../Public/js/swipe.js"          type="text/javascript"></script>
<script src="../Public/js/klass.min.js"      type="text/javascript"></script>
<!-- <script src="../Public/js/photoswipe.js"     type="text/javascript"></script> -->
<script src="../Public/js/colorbox.js"       type="text/javascript"></script>
<script src="../Public/js/retina.js"         type="text/javascript"></script>
<script src="../Public/js/modernizr.js"      type="text/javascript"></script>
<script src="../Public/js/slicebox.js"       type="text/javascript"></script>
<script src="../Public/js/twitter.js"        type="text/javascript"></script>
<script src="../Public/js/custom.js"         type="text/javascript"></script>
</head>

<body>

<div id="preloader">
    <div id="status">
        <p class="center-text">
            <em>Loading the content...</em>
            <em>Loading depends on your connection speed!</em>
        </p>
    </div>
</div>

<div class="header-decoration"></div>

<div class="nav-page-icons content">
    <a href="<?php echo U('Index/index');?>"        id="nav0" class="go-index">回到首页</a>
    <a href="<?php echo U('ShopCart/index');?>"         id="nav1" class="go-home">查看购物车</a>
    <a href="<?php echo U('Shop/index');?>"         id="nav1" class="go-home"><b>商品分类</b></a>
    <a href="<?php echo U('Favorites/index');?>"     id="nav3" class="go-features">我的最爱</a>
    <a href="<?php echo U('Member/purchase_history');?>"      id="nav4" class="go-gallery">交易记录</a>
    <a href="<?php echo U('Member/index');?>"        id="nav2" class="go-about">用户中心</a>
    <div class="nav-decoration decoration"></div>
</div>

<div class="header">
    <a href="<?php echo U('Index/index');?>" class="deploy-share"></a>
    <h4><?php echo ($title); ?></h4>
    <a href="#" class="deploy-menu"></a>
    <div class="decoration"></div>
</div>
<div class="content">
        <!--<div class="content">-->
            <!--<div class="groupBox innerContent ">-->
                <!--<div class="profileArea">-->
                    <!--<div class="profilePicAreaContainer left">-->
                        <!--<div class="profilePicArea">-->
                            <!--<img class="shadow" src="../Public/images/profile-pic-bg.png" alt="Profile Pic">-->
                            <!--<img src="../Public/images/profile-pic-2.jpg" alt="Profile Pic">-->
                        <!--</div>-->
                    <!--</div>-->
                    <!--<div class="profileInfoArea left">-->
                        <!--<div class="name">威信用户：多情的汉堡包</div>-->
                        <!--<div class="position">平台等级：白痴</div>-->
                        <!--<div class="location">当前积分：250</div>-->
                    <!--</div>-->
                    <!--<div class="clearfix"></div>-->
                <!--</div>-->
            <!--</div>-->
        <!--</div>-->

        <div class="container">
            <form action="__URL__/use_credits" method="post">


            <div class="blog-post">
                <div class="post-content">
                    <h3>您目前的可用积分为<?php echo ($hCredits); ?>，目前的兑换比例是1积分：1人民币，100积分起换。<br/>请选择您要兑换的积分数量：</h3>
                    <p>
                        <br/>
                        <select name="use_credits">
                            <option value="">请选择</option>
                        <?php
 $num = floor(($hCredits / $options['credits_exchange']) / $options['credits_start_line']); for($i = 1 ;$i <= $num; $i ++ ):?>
                            <option value="<?php echo $i * $options['credits_start_line'];?>"><?php echo $i * $options['credits_start_line'];?></option>
                        <?php endfor;?>
                        </select>
                    </p>
                </div>
                <div class="formSubmitButtonErrorsWrap">
                    <input type="submit" class="buttonWrap button grey" value="提交申请"/>
                </div>
            </form>

            </div>
        </div>

        <!--<div class="pagesMenu">-->
        <!--<a href="about.php" id="linkAbout" class="pageLink pageLink-1-1 ">-->
        <!--<div class="iconBox">-->
        <!--&lt;!&ndash;<img src="../Public/images/icon-about-2x.png" alt="Resume">&ndash;&gt;-->
        <!--<img src="../Public/images/icon-about.jpg" alt="About">-->
        <!--</div>-->
        <!--<div class="titleBox">-->
        <!--About-->
        <!--</div>-->
        <!--</a>-->

        <!--<a href="resume.php" id="linkResume" class="pageLink pageLink-1-1" >-->
        <!--<div class="iconBox">-->
        <!--&lt;!&ndash;<img src="../Public/images/icon-resume-2x.png" alt="Resume">&ndash;&gt;-->
        <!--<img src="../Public/images/icon-resume.jpg" alt="Resume">-->
        <!--</div>-->
        <!--<div class="titleBox">-->
        <!--Resume-->
        <!--</div>-->
        <!--</a>-->

        <!--<div class="clearfix"></div>-->
        <!--</div>-->
    </div>
    <div class="clearfix"></div>


    <?php if(!$noreturn):?>
<a href="javascript:history.back()" class="button-minimal grey-minimal fullscreen-button half-bottom">返回上一页</a>
<?php endif;?>
<div class="landing-footer">
    <p class="center-text footer-text">
        <br>Copyright 2013<br>konakona Design.<br>
    </p>

    <div class="footer-decoration"></div>
</div>

</body>
</html>