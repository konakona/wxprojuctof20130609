<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords"           content="" />
<meta name="Description"        content="" />
<!--Favicon shortcut link-->
<!--Declare page as mobile friendly --> 
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0"/>
<!-- Declare page as iDevice WebApp friendly -->
<meta name="apple-mobile-web-app-capable" content="yes"/>
<!-- iDevice WebApp Splash Screen, Regular Icon, iPhone, iPad, iPod Retina Icons -->
<link rel="apple-touch-icon" sizes="114x114" href="../Public/images/splash-icon.png"> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen.png" media="screen and (max-device-width: 320px)" /> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen@2x.png" media="(max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2)" /> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen@3x.png" sizes="640x1096">

<!-- Page Title -->
<title><?php echo ($title); ?></title>

<!-- Stylesheet Load -->
<link href="../Public/css/style.css"               rel="stylesheet"    type="text/css">
<link href="../Public/css/framework-style.css"     rel="stylesheet"    type="text/css">
<link href="../Public/css/framework.css"           rel="stylesheet"    type="text/css">
<link href="../Public/css/icons.css"               rel="stylesheet"    type="text/css">
<link href="../Public/css/retina.css"              rel="stylesheet"    type="text/css"     media="only screen and (-webkit-min-device-pixel-ratio: 2)" />

<!--Page Scripts Load -->
<script src="../Public/js/jquery.min.js"     type="text/javascript"></script>        
<script src="../Public/js/jquery-ui-min.js"  type="text/javascript"></script>
<script src="../Public/js/contact.js"        type="text/javascript"></script>
<script src="../Public/js/swipe.js"          type="text/javascript"></script>
<script src="../Public/js/klass.min.js"      type="text/javascript"></script>
<!-- <script src="../Public/js/photoswipe.js"     type="text/javascript"></script> -->
<script src="../Public/js/colorbox.js"       type="text/javascript"></script>
<script src="../Public/js/retina.js"         type="text/javascript"></script>
<script src="../Public/js/modernizr.js"      type="text/javascript"></script>
<script src="../Public/js/slicebox.js"       type="text/javascript"></script>
<script src="../Public/js/twitter.js"        type="text/javascript"></script>
<script src="../Public/js/custom.js"         type="text/javascript"></script>
</head>

<body>

<div id="preloader">
    <div id="status">
        <p class="center-text">
            <em>Loading the content...</em>
            <em>Loading depends on your connection speed!</em>
        </p>
    </div>
</div>

<div class="header-decoration"></div>

<div class="nav-page-icons content">
    <a href="<?php echo U('Index/index');?>"        id="nav0" class="go-index">回到首页</a>
    <a href="<?php echo U('ShopCart/index');?>"         id="nav1" class="go-home">查看购物车</a>
    <a href="<?php echo U('Shop/index');?>"         id="nav1" class="go-home"><b>商品分类</b></a>
    <a href="<?php echo U('Favorites/index');?>"     id="nav3" class="go-features">我的最爱</a>
    <a href="<?php echo U('Member/purchase_history');?>"      id="nav4" class="go-gallery">交易记录</a>
    <a href="<?php echo U('Member/index');?>"        id="nav2" class="go-about">用户中心</a>
    <div class="nav-decoration decoration"></div>
</div>

<div class="header">
    <a href="<?php echo U('Index/index');?>" class="deploy-share"></a>
    <h4><?php echo ($title); ?></h4>
    <a href="#" class="deploy-menu"></a>
    <div class="decoration"></div>
</div>
<link rel="stylesheet" href="../Public/css/member.css">
<div id="container">
    <div id="pageHome" class="page">
        <div class="content">
            <div class="groupBox innerContent ">
                <div class="profileArea">
                    <div class="profilePicAreaContainer left">
                        <div class="profilePicArea">
                            <img class="shadow" src="../Public/images/profile-pic-bg.png" alt="Profile Pic">
                            <img src="../Public/images/profile-pic-2.jpg" alt="Profile Pic">
                        </div>
                    </div>
                    <div class="profileInfoArea left">
                        <div class="name">帐号名：<?php echo ($user["phone"]); ?></div>
                        <div class="name">威信帐号：<?php echo ($user["wx_name"]); ?></div>
                        <!--<div class="position">平台等级：白痴</div>-->
                        <div class="location">当前积分：<?php echo ($hCredits); ?></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="separator"></div>


        <div class="landing-navigation">
            <div class="column">
                <a href="<?php echo U('Member/address_list');?>" class="one-third nav-home"> <em class="nav-text">地址管理</em></a>
                <a href="<?php echo U('Member/profile');?>" class="one-third nav-about"> <em
                        class="nav-text">资料修改</em></a>
</div>
            <div class="column">
                <a href="<?php echo U('Member/my_credits');?>" class="one-third nav-portfolio"> <em class="nav-text">积分兑换</em></a>
                <!--<a href="/bld_soho/index.php/Favorites/index" class="one-third nav-portfolio"> <em-->
                    <!--class="nav-text">我的最爱</em></a>-->
                <a href="<?php echo U('Member/purchase_history');?>" class="one-third nav-blog">
                <em class="nav-text">交易记录</em></a>
            </div>
        </div>

        <!--<div class="pagesMenu">-->
        <!--<a href="about.php" id="linkAbout" class="pageLink pageLink-1-1 ">-->
        <!--<div class="iconBox">-->
        <!--&lt;!&ndash;<img src="../Public/images/icon-about-2x.png" alt="Resume">&ndash;&gt;-->
        <!--<img src="../Public/images/icon-about.jpg" alt="About">-->
        <!--</div>-->
        <!--<div class="titleBox">-->
        <!--About-->
        <!--</div>-->
        <!--</a>-->

        <!--<a href="resume.php" id="linkResume" class="pageLink pageLink-1-1" >-->
        <!--<div class="iconBox">-->
        <!--&lt;!&ndash;<img src="../Public/images/icon-resume-2x.png" alt="Resume">&ndash;&gt;-->
        <!--<img src="../Public/images/icon-resume.jpg" alt="Resume">-->
        <!--</div>-->
        <!--<div class="titleBox">-->
        <!--Resume-->
        <!--</div>-->
        <!--</a>-->

        <!--<div class="clearfix"></div>-->
        <!--</div>-->
    </div>
    <div class="clearfix"></div>


    <?php if(!$noreturn):?>
<a href="javascript:history.back()" class="button-minimal grey-minimal fullscreen-button half-bottom">返回上一页</a>
<?php endif;?>
<div class="landing-footer">
    <p class="center-text footer-text">
        <br>Copyright 2013<br>konakona Design.<br>
    </p>

    <div class="footer-decoration"></div>
</div>

</body>
</html>