<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords"           content="" />
<meta name="Description"        content="" />
<!--Favicon shortcut link-->
<!--Declare page as mobile friendly --> 
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0"/>
<!-- Declare page as iDevice WebApp friendly -->
<meta name="apple-mobile-web-app-capable" content="yes"/>
<!-- iDevice WebApp Splash Screen, Regular Icon, iPhone, iPad, iPod Retina Icons -->
<link rel="apple-touch-icon" sizes="114x114" href="../Public/images/splash-icon.png"> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen.png" media="screen and (max-device-width: 320px)" /> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen@2x.png" media="(max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2)" /> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen@3x.png" sizes="640x1096">

<!-- Page Title -->
<title>Epsilon Framework</title>

<!-- Stylesheet Load -->
<link href="../Public/css/style.css"               rel="stylesheet"    type="text/css">
<link href="../Public/css/framework-style.css"     rel="stylesheet"    type="text/css">
<link href="../Public/css/framework.css"           rel="stylesheet"    type="text/css">
<link href="../Public/css/icons.css"               rel="stylesheet"    type="text/css">
<link href="../Public/css/retina.css"              rel="stylesheet"    type="text/css"     media="only screen and (-webkit-min-device-pixel-ratio: 2)" />

<!--Page Scripts Load -->
<script src="../Public/js/jquery.min.js"     type="text/javascript"></script>        
<script src="../Public/js/jquery-ui-min.js"  type="text/javascript"></script>
<script src="../Public/js/contact.js"        type="text/javascript"></script>
<script src="../Public/js/swipe.js"          type="text/javascript"></script>
<script src="../Public/js/klass.min.js"      type="text/javascript"></script>
<!-- <script src="../Public/js/photoswipe.js"     type="text/javascript"></script> -->
<script src="../Public/js/colorbox.js"       type="text/javascript"></script>
<script src="../Public/js/retina.js"         type="text/javascript"></script>
<script src="../Public/js/modernizr.js"      type="text/javascript"></script>
<script src="../Public/js/slicebox.js"       type="text/javascript"></script>
<script src="../Public/js/twitter.js"        type="text/javascript"></script>
<script src="../Public/js/custom.js"         type="text/javascript"></script>
</head>

<body>

<div id="preloader">
    <div id="status">
        <p class="center-text">
            <em>Loading the content...</em>
            <em>Loading depends on your connection speed!</em>
        </p>
    </div>
</div>

<div class="header-decoration"></div>

<div class="nav-page-icons content">
    <a href="<?php echo U('Index/index');?>"        id="nav0" class="go-index">回到首页</a>
    <a href="<?php echo U('ShopCart/index');?>"         id="nav1" class="go-home">查看购物车</a>
    <a href="<?php echo U('Shop/index');?>"         id="nav1" class="go-home"><b>商品分类</b></a>
    <a href="<?php echo U('Favorites/index');?>"     id="nav3" class="go-features">我的最爱</a>
    <a href="<?php echo U('Member/purchase_history');?>"      id="nav4" class="go-gallery">交易记录</a>
    <a href="<?php echo U('Member/index');?>"        id="nav2" class="go-about">用户中心</a>
    <div class="nav-decoration decoration"></div>
</div>

<div class="header">
    <a href="<?php echo U('Index/index');?>" class="deploy-share"></a>
    <h4><?php echo ($title); ?></h4>
    <a href="#" class="deploy-menu"></a>
    <div class="decoration"></div>
</div>
<div class="content">
    <div class="container no-bottom">
        <div class="contact-form no-bottom">


            <table cellspacing="0" class="table">
                <tbody>
                <tr>
                    <th class="table-title">订单编号</th>
                    <!--<th class="table-title">商品</th>-->
                    <th class="table-title">下单时间</th>
                    <!--<th class="table-title">发货时间</th>-->
                    <th class="table-title">状态</th>
                </tr>
                <!-- Table Header -->

                <?php if(is_array($list)): foreach($list as $key=>$vo): ?><tr>
                        <td class="table-sub-title"><?php echo ($vo["id"]); ?></td>
                        <td><?php echo (date("Y-m-d H:i",$vo["add_time"])); ?></td>
                        <!--<td>-->
                            <!--<?php if(($vo["delivery_time"]) == "0"): ?>-->
                                <!--立即-->
                                <!--<?php else: ?>-->
                                <!--<?php echo (date("Y-m-d H:i",$vo["add_time"])); ?>-->
                        <!--<?php endif; ?>-->
                        <!--</td>-->
                        <td>
                            <?php switch($vo["status"]): case "0": ?>送货中<?php break;?>
                            <?php case "1": ?>发货完成<?php break;?>
                            <?php case "2": ?>失败or作废<?php break; endswitch;?>
                        </td>
                    </tr><?php endforeach; endif; ?>
                <!--<tr class="even">-->
                    <!--<td class="table-sub-title">Price</td>-->
                    <!--<td class="price">0<sup class="small-price">99</sup> $</td>-->
                    <!--<td class="price">9<sup class="small-price">99</sup> $</td>-->
                    <!--<td class="price">19<sup class="small-price">99</sup> $</td>-->
                <!--</tr>-->
                </tbody>
            </table>


        </div>
    </div>


    <div class="decoration"></div>
</div>
<?php if(!$noreturn):?>
<a href="javascript:history.back()" class="button-minimal grey-minimal fullscreen-button half-bottom">返回上一页</a>
<?php endif;?>
<div class="landing-footer">
    <p class="center-text footer-text">
        <br>Copyright 2013<br>konakona Design.<br>
    </p>

    <div class="footer-decoration"></div>
</div>

</body>
</html>