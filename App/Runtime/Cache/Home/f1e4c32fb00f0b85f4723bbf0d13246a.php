<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords"           content="" />
<meta name="Description"        content="" />
<!--Favicon shortcut link-->
<!--Declare page as mobile friendly --> 
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0"/>
<!-- Declare page as iDevice WebApp friendly -->
<meta name="apple-mobile-web-app-capable" content="yes"/>
<!-- iDevice WebApp Splash Screen, Regular Icon, iPhone, iPad, iPod Retina Icons -->
<link rel="apple-touch-icon" sizes="114x114" href="../Public/images/splash-icon.png"> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen.png" media="screen and (max-device-width: 320px)" /> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen@2x.png" media="(max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2)" /> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen@3x.png" sizes="640x1096">

<!-- Page Title -->
<title><?php echo ($title); ?></title>

<!-- Stylesheet Load -->
<link href="../Public/css/style.css"               rel="stylesheet"    type="text/css">
<link href="../Public/css/framework-style.css"     rel="stylesheet"    type="text/css">
<link href="../Public/css/framework.css"           rel="stylesheet"    type="text/css">
<link href="../Public/css/icons.css"               rel="stylesheet"    type="text/css">
<link href="../Public/css/retina.css"              rel="stylesheet"    type="text/css"     media="only screen and (-webkit-min-device-pixel-ratio: 2)" />

<!--Page Scripts Load -->
<script src="../Public/js/jquery.min.js"     type="text/javascript"></script>        
<script src="../Public/js/jquery-ui-min.js"  type="text/javascript"></script>
<script src="../Public/js/contact.js"        type="text/javascript"></script>
<script src="../Public/js/swipe.js"          type="text/javascript"></script>
<script src="../Public/js/klass.min.js"      type="text/javascript"></script>
<!-- <script src="../Public/js/photoswipe.js"     type="text/javascript"></script> -->
<script src="../Public/js/colorbox.js"       type="text/javascript"></script>
<script src="../Public/js/retina.js"         type="text/javascript"></script>
<script src="../Public/js/modernizr.js"      type="text/javascript"></script>
<script src="../Public/js/slicebox.js"       type="text/javascript"></script>
<script src="../Public/js/twitter.js"        type="text/javascript"></script>
<script src="../Public/js/custom.js"         type="text/javascript"></script>
</head>

<body>

<div id="preloader">
    <div id="status">
        <p class="center-text">
            <em>Loading the content...</em>
            <em>Loading depends on your connection speed!</em>
        </p>
    </div>
</div>

<div class="header-decoration"></div>

<div class="nav-page-icons content">
    <a href="<?php echo U('Index/index');?>"        id="nav0" class="go-index">回到首页</a>
    <a href="<?php echo U('ShopCart/index');?>"         id="nav1" class="go-home">查看购物车</a>
    <a href="<?php echo U('Shop/index');?>"         id="nav1" class="go-home"><b>商品分类</b></a>
    <a href="<?php echo U('Favorites/index');?>"     id="nav3" class="go-features">我的最爱</a>
    <a href="<?php echo U('Member/purchase_history');?>"      id="nav4" class="go-gallery">交易记录</a>
    <a href="<?php echo U('Member/index');?>"        id="nav2" class="go-about">用户中心</a>
    <div class="nav-decoration decoration"></div>
</div>

<div class="header">
    <a href="<?php echo U('Index/index');?>" class="deploy-share"></a>
    <h4><?php echo ($title); ?></h4>
    <a href="#" class="deploy-menu"></a>
    <div class="decoration"></div>
</div>
<link href="../Public/css/other.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../Public/js/script2.js"></script>
<script type="text/javascript" src="../Public/js/jquery.cycle.min.js"></script>
<meta name="apple-mobile-web-app-capable" content="yes"/>
<meta name="apple-mobile-web-app-status-bar-style" content="black"/>
<link rel="stylesheet" type="text/css" href="../Public/css/cart.css" media="screen"/>
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<style>
    body, html {
        background-image: url(../Public/images/body-bg.png);
        -webkit-font-smoothing: antialiased;
    }

    .column h1, h2, h3, h4, h5 {
        padding-top: 5px;
    }

    h4 {
        font-size: 16px;
        line-height: 20px;
        padding-bottom: 1px;
    }

    a {
        font-size: 14px;
    }
</style>

<!-- <link href="../Public/css/jquery-ui.css"               rel="stylesheet"    type="text/css"> -->
<!-- <script type="text/javascript" src="../Public/js/jquery-ui.js" ></script> -->
<div class="content">

    <!-- 适合4x广告哟 -->
    <!--     <div class="container">
            <h3>Advertise</h3>
            <p>Want to join the program? Go to <a href="#">this page</a> and sign up today! We'll get back to you!</p>
            <div class="ads">
                <img src="../Public/images/tf_125x125_v1.gif" alt="img">
                <img src="../Public/images/tf_125x125_v1.gif" alt="img">
                <img src="../Public/images/tf_125x125_v1.gif" alt="img">
                <img src="../Public/images/tf_125x125_v1.gif" alt="img">
            </div>
        </div> -->


    <div id="posts">

        <?php if(is_array($list)): foreach($list as $key=>$vo): ?><div class="post">
                <div class="post-title">
                    <h2><a class="open-close" href="#"><?php echo ($vo["name"]); ?>-<?php echo ($vo["specifications"]); ?>(<?php echo ($vo["price"]); ?>元/<?php echo ($vo["unit"]); ?>) </a></h2>
                    <a href="__APP__/Favorites" shop_id=<?php echo ($vo["id"]); ?> class="more"></a>
                    <a href="<?php echo U('ShopCart/add',array('id'=>$vo[id]));?>" class="buy"></a>
                </div>
                <div class="post-content" style="display: none;">
                    <div class="post-meta">
                        <a class="imgbox"><img width="101" height="70"
                                               src="<?php echo ($vo["img"]); ?>"></a>
                        <!-- <p>商品种类: <a href="#">水</a></p> -->
                        <p><?php echo ($vo["content"]); ?></p>
                        <!-- <p>Date: <a href="singlepost.html">22nd Nov 2010</a></p> -->
                        <!-- <a href="singlepost.html" class="comments-bubble">已有多少人收藏</a> -->
                    </div>
                    <!-- /.post-meta -->
                    <!-- <p>商品信息...</p> -->
                    <!-- <a href="singlepost.html" class="morebtn">Read More</a> -->
                </div>
            </div>
            <!-- /.post --><?php endforeach; endif; ?>
    </div>

    <?php echo ($page); ?>

    <div class="decoration"></div>

    <aside>

        <!-- Cart incl. Summary, Product List & View Cart Link -->
        <section id="cart">
            <h2><a href="<?php echo U('ShopCart/index');?>" title="View Cart"><img
                    src="../Public/images/icon-cart.png" alt="Cart"></a>购物车</h2>

            <p>您的购物车里有 <b><?php echo (count($shop_cart)); ?> 个商品</b> ，一共是 <b><?php echo ($total_price); ?>￥</b></p>

            <p class="right"><a href="<?php echo U('ShopCart/index');?>" title="Proceed to Checkout"><b>查看购物车</b></a></p>
        </section>

    </aside>

    <div class="container no-bottom">
        <h3>商品分类</h3>
        <br/>
        <style>
            .b1 {
                margin-right: 0px;
                margin-left: 0px;
                height: 30px;
                line-height: 15px;
                display: inline;
                float: left;
            }
        </style>


        <div class="column no-bottom">
            <!-- <a href="index.html" class="button-minimal grey-minimal fullscreen-button half-bottom" style="margin-bottom: 0px!important;">饮料</a> -->

            <?php if(is_array($cate_list)): foreach($cate_list as $key=>$vo): ?><h4 class="icon-heading" style="<?php if($_GET['cate'] == $vo['id']) echo 'background-color: yellowgreen';?>"><a href="__URL__/index/cate/<?php echo ($vo["id"]); ?>" style="<?php if($_GET['cate'] == $vo['id']) echo 'font-weight: bold';?>"><?php echo ($vo["name"]); ?></a></h4>

                <div class="decoration"></div><?php endforeach; endif; ?>
        </div>
    </div>
</div>

<?php if(!$noreturn):?>
<a href="javascript:history.back()" class="button-minimal grey-minimal fullscreen-button half-bottom">返回上一页</a>
<?php endif;?>
<div class="landing-footer">
    <p class="center-text footer-text">
        <br>Copyright 2013<br>konakona Design.<br>
    </p>

    <div class="footer-decoration"></div>
</div>

</body>
</html>