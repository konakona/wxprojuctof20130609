<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords"           content="" />
<meta name="Description"        content="" />
<!--Favicon shortcut link-->
<!--Declare page as mobile friendly --> 
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0"/>
<!-- Declare page as iDevice WebApp friendly -->
<meta name="apple-mobile-web-app-capable" content="yes"/>
<!-- iDevice WebApp Splash Screen, Regular Icon, iPhone, iPad, iPod Retina Icons -->
<link rel="apple-touch-icon" sizes="114x114" href="../Public/images/splash-icon.png"> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen.png" media="screen and (max-device-width: 320px)" /> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen@2x.png" media="(max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2)" /> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen@3x.png" sizes="640x1096">

<!-- Page Title -->
<title>Epsilon Framework</title>

<!-- Stylesheet Load -->
<link href="../Public/css/style.css"               rel="stylesheet"    type="text/css">
<link href="../Public/css/framework-style.css"     rel="stylesheet"    type="text/css">
<link href="../Public/css/framework.css"           rel="stylesheet"    type="text/css">
<link href="../Public/css/icons.css"               rel="stylesheet"    type="text/css">
<link href="../Public/css/retina.css"              rel="stylesheet"    type="text/css"     media="only screen and (-webkit-min-device-pixel-ratio: 2)" />

<!--Page Scripts Load -->
<script src="../Public/js/jquery.min.js"     type="text/javascript"></script>        
<script src="../Public/js/jquery-ui-min.js"  type="text/javascript"></script>
<script src="../Public/js/contact.js"        type="text/javascript"></script>
<script src="../Public/js/swipe.js"          type="text/javascript"></script>
<script src="../Public/js/klass.min.js"      type="text/javascript"></script>
<!-- <script src="../Public/js/photoswipe.js"     type="text/javascript"></script> -->
<script src="../Public/js/colorbox.js"       type="text/javascript"></script>
<script src="../Public/js/retina.js"         type="text/javascript"></script>
<script src="../Public/js/modernizr.js"      type="text/javascript"></script>
<script src="../Public/js/slicebox.js"       type="text/javascript"></script>
<script src="../Public/js/twitter.js"        type="text/javascript"></script>
<script src="../Public/js/custom.js"         type="text/javascript"></script>
</head>

<body>

<div id="preloader">
    <div id="status">
        <p class="center-text">
            <em>Loading the content...</em>
            <em>Loading depends on your connection speed!</em>
        </p>
    </div>
</div>

<div class="header-decoration"></div>

<div class="nav-page-icons content">
    <a href="index.html"        id="nav0" class="go-index">回到首页</a>
    <a href="about.html"        id="nav2" class="go-about">账户信息</a>
    <a href="home.html"         id="nav1" class="go-home"><b>商品分类</b></a>
    <a href="home.html"         id="nav1" class="go-home">热卖促销</a>
    <a href="features.html"     id="nav3" class="go-features">我的最爱</a>
    <a href="gallery.html"      id="nav4" class="go-gallery">交易记录</a>
    <div class="nav-decoration decoration"></div>
</div>

<div class="header">
    <a href="index.html" class="deploy-share"></a>
    <h4><?php echo ($title); ?></h4>
    <a href="#" class="deploy-menu"></a>
    <div class="decoration"></div>
</div>
<link href="../Public/css/other.css"               rel="stylesheet"    type="text/css">
<script type="text/javascript" src="../Public/js/script2.js" ></script>
<script type="text/javascript" src="../Public/js/jquery.cycle.min.js" ></script>
<!-- <link href="../Public/css/jquery-ui.css"               rel="stylesheet"    type="text/css"> -->
<!-- <script type="text/javascript" src="../Public/js/jquery-ui.js" ></script> -->
<div class="content">
    <!-- 可以作为新闻内页之类的 -->
<!--     <div class="decoration"></div>
    <div class="container">
        <div class="blog-post">
            <div class="post-image">
                <img class="blog-post-image" src="../Public/images/001.jpg" alt="img">  
            </div>
            <div class="post-content">
                <h3>Blog Post Title</h3>         
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                    Lorem Ipsum has been the industry's standard text.

                </p>
            </div>
            <div class="post-details">
                <a class="posted-by" href="#">admin</a>
                <a class="posted-at" href="#">2 days ago</a>
                <a class="posted-cat" href="#">HTML5</a>
                <a class="posted-tag" href="#">frameworks, development</a>
                <a class="posted-more" href="blog-post.html">Read More</a>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    
    <div class="decoration"></div> -->
 
    <!-- 可以做顶部打广告 -->
<!--     <div class="container">
        <div class="blog-post">
            <h3>Blog Post Title</h3> 
            <img class="blog-post-image" src="../Public/images/1.jpg" alt="img">       
            <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                Lorem Ipsum has been the industry's standard text.
            </p>
            <div class="post-details">
                <a class="posted-by" href="#">admin</a>
                <a class="posted-at" href="#">2 days ago</a>
                <a class="posted-cat" href="#">HTML5</a>
                <a class="posted-tag" href="#">frameworks, development</a>
                <a class="posted-more" href="blog-post.html">Read More</a>
                <div class="clear"></div>
            </div>
        </div>
    </div>  -->
        
    <div class="decoration"></div>   
 
    <!-- 适合4x广告哟 -->
<!--     <div class="container">
        <h3>Advertise</h3>
        <p>Want to join the program? Go to <a href="#">this page</a> and sign up today! We'll get back to you!</p>
        <div class="ads">
            <img src="../Public/images/tf_125x125_v1.gif" alt="img">
            <img src="../Public/images/tf_125x125_v1.gif" alt="img">
            <img src="../Public/images/tf_125x125_v1.gif" alt="img">
            <img src="../Public/images/tf_125x125_v1.gif" alt="img">
        </div>      
    </div> -->


    <div id="posts">
    
    <?php for($i = 0; $i < 10; $i ++ ):?>
        <div class="post">
            <div class="post-title">
                <h2><a class="open-close" href="#">可口可乐 (2.5元/瓶) </a></h2>
                <a href="#" class="more"></a>
                <a href="#" class="buy"></a>
            </div>
            <div class="post-content" style="display: none;">
                <div class="post-meta">
                    <a class="imgbox"><img width="101" height="70" src="../Public/images/shop/yinliao/00001.jpg"></a>
                    <!-- <p>商品种类: <a href="#">水</a></p> -->
                    <p>商品信息...
                    商品信息...
                商品信息...
            商品信息...
        商品信息...
    商品信息...</p>
                    <!-- <p>Date: <a href="singlepost.html">22nd Nov 2010</a></p> -->
                    <!-- <a href="singlepost.html" class="comments-bubble">已有多少人收藏</a> -->
                </div><!-- /.post-meta -->
                <!-- <p>商品信息...</p> -->
                <!-- <a href="singlepost.html" class="morebtn">Read More</a> -->
            </div>
        </div><!-- /.post -->
    </div>

<?php endfor;?>


    <div class="container">
        <a href="#" class="button grey" id="filter-one">1</a>
        <a href="#" class="button grey" id="filter-two">2</a>
        <a href="#" class="button grey" id="filter-three">3</a>
        <a href="#" class="button grey" id="filter-all">下一页</a>
    </div>
    
    <div class="decoration"></div>

        <div class="container no-bottom">
        <h3>商品分类</h3>
        <style>
        .b1{
            margin-right: 0px;margin-left:0px;height:30px;line-height:15px;
            display:inline;
            float:left;
        }
        </style>
        <div class="column no-bottom">
                <!-- <a href="index.html" class="button-minimal grey-minimal fullscreen-button half-bottom" style="margin-bottom: 0px!important;">饮料</a> -->

                <h4 class="icon-heading"><a href="" style="" class="">饮料</a></h4>
                <div class="decoration"></div>
                <h4 class="icon-heading"><a href="" style="" class="">饮料</a></h4>
        </div>
    </div>   

    <div class="decoration"></div>

</div>

<div class="landing-footer">
	<p class="center-text footer-text">
    <br>Copyright 2013<br>konakona Design.<br>
    </p>
	<div class="footer-decoration"></div>
</div>



</body>
</html>