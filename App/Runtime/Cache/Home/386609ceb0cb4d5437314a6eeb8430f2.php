<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords"           content="" />
<meta name="Description"        content="" />
<!--Favicon shortcut link-->
<!--Declare page as mobile friendly --> 
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0"/>
<!-- Declare page as iDevice WebApp friendly -->
<meta name="apple-mobile-web-app-capable" content="yes"/>
<!-- iDevice WebApp Splash Screen, Regular Icon, iPhone, iPad, iPod Retina Icons -->
<link rel="apple-touch-icon" sizes="114x114" href="../Public/images/splash-icon.png"> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen.png" media="screen and (max-device-width: 320px)" /> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen@2x.png" media="(max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2)" /> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen@3x.png" sizes="640x1096">

<!-- Page Title -->
<title>Epsilon Framework</title>

<!-- Stylesheet Load -->
<link href="../Public/css/style.css"               rel="stylesheet"    type="text/css">
<link href="../Public/css/framework-style.css"     rel="stylesheet"    type="text/css">
<link href="../Public/css/framework.css"           rel="stylesheet"    type="text/css">
<link href="../Public/css/icons.css"               rel="stylesheet"    type="text/css">
<link href="../Public/css/retina.css"              rel="stylesheet"    type="text/css"     media="only screen and (-webkit-min-device-pixel-ratio: 2)" />

<!--Page Scripts Load -->
<script src="../Public/js/jquery.min.js"     type="text/javascript"></script>        
<script src="../Public/js/jquery-ui-min.js"  type="text/javascript"></script>
<script src="../Public/js/contact.js"        type="text/javascript"></script>
<script src="../Public/js/swipe.js"          type="text/javascript"></script>
<script src="../Public/js/klass.min.js"      type="text/javascript"></script>
<!-- <script src="../Public/js/photoswipe.js"     type="text/javascript"></script> -->
<script src="../Public/js/colorbox.js"       type="text/javascript"></script>
<script src="../Public/js/retina.js"         type="text/javascript"></script>
<script src="../Public/js/modernizr.js"      type="text/javascript"></script>
<script src="../Public/js/slicebox.js"       type="text/javascript"></script>
<script src="../Public/js/twitter.js"        type="text/javascript"></script>
<script src="../Public/js/custom.js"         type="text/javascript"></script>
</head>

<body>

<div id="preloader">
    <div id="status">
        <p class="center-text">
            <em>Loading the content...</em>
            <em>Loading depends on your connection speed!</em>
        </p>
    </div>
</div>

<div class="header-decoration"></div>

<div class="nav-page-icons content">
    <a href="<?php echo U('Index/index');?>"        id="nav0" class="go-index">回到首页</a>
    <a href="<?php echo U('ShopCart/index');?>"         id="nav1" class="go-home">查看购物车</a>
    <a href="<?php echo U('Shop/index');?>"         id="nav1" class="go-home"><b>商品分类</b></a>
    <a href="<?php echo U('Favorites/index');?>"     id="nav3" class="go-features">我的最爱</a>
    <a href="<?php echo U('Member/purchase_history');?>"      id="nav4" class="go-gallery">交易记录</a>
    <a href="<?php echo U('Member/index');?>"        id="nav2" class="go-about">用户中心</a>
    <div class="nav-decoration decoration"></div>
</div>

<div class="header">
    <a href="<?php echo U('Index/index');?>" class="deploy-share"></a>
    <h4><?php echo ($title); ?></h4>
    <a href="#" class="deploy-menu"></a>
    <div class="decoration"></div>
</div>
<script type="text/javascript" src="../Public/js/script2.js"></script>
<script type="text/javascript" src="../Public/js/jquery.cycle.min.js"></script>
<meta name="apple-mobile-web-app-capable" content="yes"/>
<meta name="apple-mobile-web-app-status-bar-style" content="black"/>
<link rel="stylesheet" type="text/css" href="../Public/css/cart.css" media="screen"/>
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<style>
    /*body, html {*/
        /*background-image: url(../Public/images/body-bg.png);*/
        /*-webkit-font-smoothing: antialiased;*/
    /*}*/

    a {
        font-size: 12px;
    }
</style>

<section id="content">

    <section id="left-column">

        <!-- Main Content Area -->
        <section id="main">

            <h2>购物车查看</h2>

            <p>您的购买了以下物品:</p>

            <form method="post" action="__SELF__">


                <?php if(is_array($shop_cart)): foreach($shop_cart as $key=>$vo): ?><div class="product-cart">

                    <h2><?php echo ($vo["name"]); ?>/<?php echo ($vo["unit"]); echo ($vo["price"]); ?>￥</h2>

                    <!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor...</p>-->
                    <label for="amount-product-1">数量 <a href="__URL__/delete/id/<?php echo ($vo["id"]); ?>"  style="display:inline" title="Remove">(删除)</a></label>
                    <input type="text" name="num[<?php echo ($vo["id"]); ?>]" style="display:inline" id="amount-product-1" value="<?php echo ($vo["num"]); ?>" size="2"/>

                    <h2 class="item"><?php echo $vo[price] * $vo[num] ;?>￥</h2><input type="submit" name="update" class="button" value="更改"/>
                </div><?php endforeach; endif; ?>


                <div class="total">
                    <h3>总价: <b><?php echo ($total_price); ?>￥</b></h3>
                    <!--<br/>(可获积分255点)-->
                </div>
                <div class="right">
                    <input type="submit" name="submit" class="button" value="下一步"/>
                </div>
            </form>

        </section>
        <!-- Secondary Feature Area — Useful for Sales -->
        <section class="sale">
            <h1>秒杀商品<a href="<?php echo U('Shop/index',array('type'=>2));?>" title="See All Products">查看全部</a></h1>
            <ul>
                <li class="slide">

                    <?php if(is_array($shop_list)): foreach($shop_list as $key=>$vo): ?><div class="product">
                        <img src="http://<?php echo ($THINK["SERVER"]["HTTP_HOST"]); ?>/<?php echo ($vo["img"]); ?>" alt="Product Image"/>

                        <div class="overlay">
                            <div class="price">
                                <h2><?php echo ($vo["name"]); ?></h2>
                                <ul>
                                    <li><b><?php echo ($vo["price"]); ?>￥</b>
                                        <del><?php echo ($vo["price"]); ?>￥</del>
                                    </li>
                                    <li class="details"><a href="<?php echo U('ShopCart/add',array('id'=>$vo[id]));?>"  class="buy">预定</a></li>
                                </ul>
                            </div>
                        </div>
                    </div><?php endforeach; endif; ?>
                </li>
            </ul>
        </section>

    </section>
</section>

<?php if(!$noreturn):?>
<a href="javascript:history.back()" class="button-minimal grey-minimal fullscreen-button half-bottom">返回上一页</a>
<?php endif;?>
<div class="landing-footer">
    <p class="center-text footer-text">
        <br>Copyright 2013<br>konakona Design.<br>
    </p>

    <div class="footer-decoration"></div>
</div>

</body>
</html>