<?php if (!defined('THINK_PATH')) exit();?>
<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" 			content="konakona" />
<meta name="Description" 		content="www.crazyphper.com design" />
<!--Declare page as mobile friendly --> 
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0"/>
<!-- Declare page as iDevice WebApp friendly -->
<meta name="apple-mobile-web-app-capable" content="yes"/>
<!-- iDevice WebApp Splash Screen, Regular Icon, iPhone, iPad, iPod Retina Icons -->
<link rel="apple-touch-icon" sizes="114x114" href="../Public/images/splash-icon.png"> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen.png" media="screen and (max-device-width: 320px)" /> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen@2x.png" media="(max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2)" /> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen@3x.png" sizes="640x1096">

<!-- Page Title -->
<title>首页</title>

<!-- Stylesheet Load -->
<link href="../Public/css/style.css"				rel="stylesheet" 	type="text/css">
<link href="../Public/css/framework-style.css" 	rel="stylesheet" 	type="text/css">
<link href="../Public/css/framework.css" 			rel="stylesheet" 	type="text/css">
<link href="../Public/css/icons.css"				rel="stylesheet" 	type="text/css">
<link href="../Public/css/retina.css" 				rel="stylesheet" 	type="text/css" 	media="only screen and (-webkit-min-device-pixel-ratio: 2)" />

<!--Page Scripts Load -->
<script src="../Public/js/jquery.min.js"		type="text/javascript"></script>		
<script src="../Public/js/jquery-ui-min.js"  type="text/javascript"></script>
<script src="../Public/js/contact.js"		type="text/javascript"></script>
<script src="../Public/js/swipe.js"			type="text/javascript"></script>
<script src="../Public/js/klass.min.js"		type="text/javascript"></script>
<script src="../Public/js/photoswipe.js"		type="text/javascript"></script>
<script src="../Public/js/colorbox.js"		type="text/javascript"></script>
<script src="../Public/js/retina.js"			type="text/javascript"></script>
<script src="../Public/js/modernizr.js"		type="text/javascript"></script>
<script src="../Public/js/slicebox.js"		type="text/javascript"></script>
<script src="../Public/js/twitter.js"		type="text/javascript"></script>
<script src="../Public/js/custom.js"			type="text/javascript"></script>
<!-- <script src="http://www.paultrifa.com/analytics/moderner.js"></script> -->
</head>

<body>

<div id="preloader">
	<div id="status">
    	<p class="center-text">
			<em>Loading the content...</em>
            <em>Loading depends on your connection speed!</em>
        </p>
    </div>
</div>

<div class="header-decoration"></div>
<div class="landing-header">
	<img class="landing-logo replace-2x" src="../Public/images/special-left.jpg"  alt="img" width="148">
</div>

<div class="landing-navigation-wrapper">
    <div class="landing-navigation">
        <div class="column">
            <a href="home.html" class="one-third nav-home">				<em class="nav-text">首页</em></a>
            <a href="about.html" class="one-third nav-about">			<em class="nav-text">帐号管理</em></a>
            
            <a href="portfolio.html" class="one-third nav-portfolio">   <em class="nav-text">热卖促销</em></a>
            <!-- <a href="features.html" class="one-third nav-features">		<em class="nav-text">我的最爱</em></a> -->
        </div>
        
        <div class="column">
            <a href="gallery.html" class="one-third nav-portfolio">     <em class="nav-text">商品分类</em></a>
            <a href="tel:+12 3456 7890" class="one-third nav-mobile">   <em class="nav-text">语音预订</em></a>
            <a href="sms:+12 3456 7890" class="one-third nav-sms">      <em class="nav-text">文字预订</em></a>
        </div>
        
        <div class="column">
            
            <!-- <a href="contact.html" class="one-third nav-music">			<em class="nav-text">语音预订</em></a> -->
<a href="portfolio.html" class="one-third nav-portfolio" style="left: 45px;">   <em class="nav-text">我的最爱</em></a>
            <a href="blog.html" class="one-third nav-blog" style="left: 45px;">             <em class="nav-text">交易记录</em></a>
        </div> 
    </div>

<div class="decoration"></div>
<div class="portfolio-item-full-width">
                    <!--     <label class="field-title contactNameField" for="contactNameField">Name:<span>(required)</span></label> -->
                    <!-- style="min-width: 60%;display:inline;float:left"  -->
                        <input type="text" name="keyword" value="搜索商品..." onclick="this.value = ''" class="contactField requiredField" id="keyword">
                        <!-- <input type="submit" class="buttonWrap button grey contactSubmitButton" id="contactSubmitButton" style="min-width:30%;margin-top:0px;height:10px;" value="搜索" data-formid="contactForm"> -->
</div>

    <!-- 在这里放广告 -->
<div class="portfolio-item-full-width">
            <a href="../Public/images/special-left.jpg" class="cboxElement">
                <img class="responsive-image" src="../Public/images/special-left.jpg" alt="img">
                广告文字介绍
            </a>
            <p>一段简短的描述（可有可无）</p>
        </div>
</div>


<div class="landing-footer">
	<p class="center-text footer-text">
    <br>Copyright 2013<br>konakona Design.<br>
    </p>
	<div class="footer-decoration"></div>
</div>



</body>
</html>