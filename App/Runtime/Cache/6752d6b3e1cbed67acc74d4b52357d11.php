<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords"           content="" />
<meta name="Description"        content="" />
<!--Favicon shortcut link-->
<!--Declare page as mobile friendly --> 
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0"/>
<!-- Declare page as iDevice WebApp friendly -->
<meta name="apple-mobile-web-app-capable" content="yes"/>
<!-- iDevice WebApp Splash Screen, Regular Icon, iPhone, iPad, iPod Retina Icons -->
<link rel="apple-touch-icon" sizes="114x114" href="../Public/images/splash-icon.png"> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen.png" media="screen and (max-device-width: 320px)" /> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen@2x.png" media="(max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2)" /> 
<link rel="apple-touch-startup-image" href="../Public/images/splash-screen@3x.png" sizes="640x1096">

<!-- Page Title -->
<title>Epsilon Framework</title>

<!-- Stylesheet Load -->
<link href="../Public/css/style.css"               rel="stylesheet"    type="text/css">
<link href="../Public/css/framework-style.css"     rel="stylesheet"    type="text/css">
<link href="../Public/css/framework.css"           rel="stylesheet"    type="text/css">
<link href="../Public/css/icons.css"               rel="stylesheet"    type="text/css">
<link href="../Public/css/retina.css"              rel="stylesheet"    type="text/css"     media="only screen and (-webkit-min-device-pixel-ratio: 2)" />

<!--Page Scripts Load -->
<script src="../Public/js/jquery.min.js"     type="text/javascript"></script>        
<script src="../Public/js/jquery-ui-min.js"  type="text/javascript"></script>
<script src="../Public/js/contact.js"        type="text/javascript"></script>
<script src="../Public/js/swipe.js"          type="text/javascript"></script>
<script src="../Public/js/klass.min.js"      type="text/javascript"></script>
<!-- <script src="../Public/js/photoswipe.js"     type="text/javascript"></script> -->
<script src="../Public/js/colorbox.js"       type="text/javascript"></script>
<script src="../Public/js/retina.js"         type="text/javascript"></script>
<script src="../Public/js/modernizr.js"      type="text/javascript"></script>
<script src="../Public/js/slicebox.js"       type="text/javascript"></script>
<script src="../Public/js/twitter.js"        type="text/javascript"></script>
<script src="../Public/js/custom.js"         type="text/javascript"></script>
</head>

<body>

<div id="preloader">
    <div id="status">
        <p class="center-text">
            <em>Loading the content...</em>
            <em>Loading depends on your connection speed!</em>
        </p>
    </div>
</div>

<div class="header-decoration"></div>

<div class="nav-page-icons content">
    <a href="index.html"        id="nav0" class="go-index">回到首页</a>
    <a href="about.html"        id="nav2" class="go-about">账户信息</a>
    <a href="home.html"         id="nav1" class="go-home"><b>商品分类</b></a>
    <a href="home.html"         id="nav1" class="go-home">热卖促销</a>
    <a href="features.html"     id="nav3" class="go-features">我的最爱</a>
    <a href="gallery.html"      id="nav4" class="go-gallery">交易记录</a>
    <div class="nav-decoration decoration"></div>
</div>

<div class="header">
    <a href="index.html" class="deploy-share"></a>
    <h4><?php echo ($title); ?></h4>
    <a href="#" class="deploy-menu"></a>
    <div class="decoration"></div>
</div>
<div class="content">
    <div class="decoration"></div>
	<div class="container no-bottom">
        <div class="contact-form no-bottom"> 
            <div class="formSuccessMessageWrap" id="formSuccessMessageWrap">
                <div class="notification-box green-box">
                    <h4>YOUR MESSAGE HAS BEEN SENT!</h4>
                    <a href="#" class="close-notification">x</a>
                    <div class="clear"></div>
                    <p>
                        You're message has been successfully sent. Please allow up to 48 hours for us to reply!  
                    </p>  
                </div> 
            </div>
        
            <form action="php/contact.php" method="post" class="contactForm" id="contactForm">
                <fieldset>
                    <span class="formValidationError" id="contactNameFieldError">
                        <div class="small-notification red-notification">
                            <p>Error Sending! Please enter your name!</p>
                        </div>
                    </span>             
                    <span class="formValidationError" id="contactEmailFieldError">
                        <div class="small-notification red-notification">
                            <p>Error Sending! Mail address required!</p>
                        </div>
                    </span> 
                    <span class="formValidationError" id="contactEmailFieldError2">
                        <div class="small-notification red-notification">
                            <p>Error Sending! Valid Email Address Required</p>
                        </div>
                    </span> 
                    <span class="formValidationError" id="contactMessageTextareaError">
                        <div class="small-notification red-notification">
                            <p>Error Sending! Message field is empty!</p>
                        </div>
                    </span>  
                    <div class="formFieldWrap">
                        <label class="field-title contactNameField" for="contactNameField">Name:<span>(required)</span></label>
                        <input type="text" name="contactNameField" value="" class="contactField requiredField" id="contactNameField"/>
                    </div>
                    <div class="formFieldWrap">
                        <label class="field-title contactEmailField" for="contactEmailField">Email: <span>(required)</span></label>
                        <input type="text" name="contactEmailField" value="" class="contactField requiredField requiredEmailField" id="contactEmailField"/>
                    </div>
                    <div class="formTextareaWrap">
                        <label class="field-title contactMessageTextarea" for="contactMessageTextarea">Message: <span>(required)</span></label>
                        <textarea name="contactMessageTextarea" class="contactTextarea requiredField" id="contactMessageTextarea"></textarea>
                    </div>
                    <div class="formSubmitButtonErrorsWrap">
                        <input type="submit" class="buttonWrap button grey contactSubmitButton" id="contactSubmitButton" value="提交注册" data-formId="contactForm"/>
                    </div>
                </fieldset>
            </form>       
        </div>
    </div>
	<div class="decoration"></div>
</div>
<div class="landing-footer">
	<p class="center-text footer-text">
    <br>Copyright 2013<br>konakona Design.<br>
    </p>
	<div class="footer-decoration"></div>
</div>



</body>
</html>