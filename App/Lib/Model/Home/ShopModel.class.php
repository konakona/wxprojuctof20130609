<?php
/**
 *
 * User: konakona http://www.crazyphper.com
 * Date: 13-5-19
 * Time: 下午11:07
 */
class ShopModel extends Model
{
	/**
	 * @param $id
	 * @return string
	 */
	public function getCateName($id)
	{
		if (!is_numeric($id)) return false;
		$this->where(array('id' => $id))->getField('name');
	}

	protected function getKeyWordStr(&$map){
		if($_GET['keyword']!='搜索商品...' && $_GET['keyword']!=''){
			$map['name'] = array('like','%'.$_GET['keyword'].'%');
		}
	}

	/**
	 * @param $cateID
	 * @param $typeID
	 * @param string $firstRow
	 * @param string $listRows
	 * @return array
	 */
	public function getList($cateID, $typeID = 1, $firstRow = '', $listRows = '')
	{
		$map = array();
		$map['type'] = $typeID;
		if ($cateID > 0) $map['cate_id'] = $cateID;

		$this->getKeyWordStr($map);

		if (!$firstRow && !$listRows)
		{
			return $this->where($map)->count();
		} else
		{
			return $this->where($map)->limit($firstRow . ',' . $listRows)->order('id desc')->select();
//			echo $this->getLastSql();
		}
	}



	public function getCate() {
		return M('ShopCate')->order('id desc')->select();
	}

	/**
	 * 增加销量
	 * @param $id
	 * @param $num
	 */
	public function addSaleNum($id, $num)
	{
		$this->where('id=' . $id)->setInc('sale_num', $num);
	}

	/**
	 * 减少商品的库存
	 * @param $id
	 * @param $num
	 */
	public function incNum($id,$num){
		if($this->where('id='.$id)->getField('num') > 0)
			$this->where('id=' . $id)->setDec('num', $num);
	}
}
