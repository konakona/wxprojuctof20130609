<?php
/**
 *
 * User: konakona http://www.crazyphper.com
 * Date: 13-5-22
 * Time: 上午11:32
 */
class ShopCartModel extends Model
{
	public function getMine($uid)
	{
		$result = $this->where(array('uid' => $uid,'type'=>0))->select();
//		echo $this->getLastSql();
		if ($result)
		{
			foreach ($result as $k => $v)
			{
				$shop = D('Shop')->where(array('id' => $v['shop_id']))->find();
//				echo D('Shop')->getLastSql();
				$result[$k]['name'] = $shop['name'];
				$result[$k]['unit'] = $shop['unit'];
				$result[$k]['type'] = $shop['type'];
				$result[$k]['price'] = $shop['price'];
//				$result[$k]['num'] = $shop['num'];
				unset($shop);
			}
		}
		return $result;
	}

	public function getTotalPrice($data)
	{
		$totalPrice = 0;
		foreach ($data as $k => $v)
		{
			$totalPrice += $v['price'] * $v['num'];
		}

		return $totalPrice;
	}
}
