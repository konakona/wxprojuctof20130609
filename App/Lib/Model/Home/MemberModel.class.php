<?php
/**
 *
 * User: konakona http://www.crazyphper.com
 * Date: 13-5-20
 * Time: 下午7:37
 */
class MemberModel extends Model
{
	protected $_validate  = array(
//		array('username','require','请输入商品名称！',1,'',MODEL::MODEL_INSERT),
		array('name','require','您的联系姓名未填！',1,),
		array('wx_name','require','您的威信帐号未填！',1,),
		array('phone','number','请输入您的手机号（作为登录帐号使用）！',1),
		array('address','require','请输入您的收货地址！',1),
		array('password','require','请输入您的登录密码！',1),
	);

	protected $_auto = array(
		array('add_time',NOW_TIME,MODEL::MODEL_INSERT),
		array('last_login_time',NOW_TIME,MODEL::MODEL_UPDATE),
		array('password','md5',MODEL::MODEL_BOTH,'function'),
	);

	/**
	 * 根据倍率增加会员的积分
	 * @param $uid
	 * @param $totalPrice
	 * @param $ratio
	 */
	public function addCredits($uid,$totalPrice,$ratio){
		$this->where(array('id'=>$uid))->setInc('credits',$totalPrice * $ratio);
	}

	/**
	 * 升级值
	 * @param $uid
	 * @param $totalPrice
	 */
	public function addLevelVal($uid,$totalPrice){
		$this->where(array('id'=>$uid))->setInc('level_num',$totalPrice);
	}
}