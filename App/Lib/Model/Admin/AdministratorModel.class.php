<?php

class AdministratorModel extends CommonModel{
	    public $_validate	=	array(
        array('username','/^[a-z]\w{3,}$/i','帐号格式错误'),
        array('password','require','密码必须',2),
        array('nickname','require','昵称必须'),
        array('repassword','require','确认密码必须',2),
        array('repassword','password','确认密码不一致',2,'confirm'),
        array('username','','帐号已经存在',self::EXISTS_VALIDATE,'unique',self::MODEL_INSERT),
        );

    public $_auto		=	array(
//        array('password','pwdHash',self::MODEL_BOTH,'callback'),
        array('status',1),
        array('create_time','time',self::MODEL_INSERT,'function'),
        array('update_time','time',self::MODEL_UPDATE,'function'),
        array('last_login_time','time',2,'function'),
        array('last_login_ip','get_client_ip',2,'function'),
        );

    public function pwdHash($salt,$password) {
//        $salt = self::generateSalt($_POST['password']);
        return md5(sha1($salt.$password));
    }

    public static function generateSalt() {
        return $salt = substr(uniqid(rand()), -6);
    }

}