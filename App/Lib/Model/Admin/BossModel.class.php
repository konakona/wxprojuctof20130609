<?php

class BossModel extends CommonModel{
	protected $tableName = 'boss_info';

	protected $_validate  = array(
		array('name','require','请输入便利店名称！',1,'',MODEL::MODEL_INSERT),
		array('name','','该便利店已经存在！',1,'unique',1),
//		array('admin_id','number','帐号创建失败！',1),
		array('address','require','请输入您便利店的地址！',1),
		array('phone','number','请输入联系手机！',1),
		array('main_page','require','请选择首页模版！',1,'',MODEL::MODEL_UPDATE),
		array('mark','require','请输入一个独有的标识！',1),
	);

    /**
     * 保存数据
     *
     * @param $post
     *
     * @return bool
     */
    public function saveData( $post ) {

        if ( $this->create( $post ) === false ) {
            return $this->getError();
        }

        $adminData = $post;
        $adminData[ 'nickname' ] = $post[ 'name' ];
        $adminData['id'] = $post['admin_id'];

        $adminModel = D( 'Administrator' );

        if ( $post[ 'id' ] > 0 ) {

            if ( $adminData[ 'password' ] == "" ) {
                unset($adminData['password']);
            }

            if ( false === $adminModel->create( $adminData ) ) {
                return $adminModel->getError();
            }

            //检查密码也没有改动
            if ( $adminData[ 'password' ] ) {
                if ( $adminData[ 'password' ] == $adminData[ 'repassword' ] && $adminData[ 'password' ] ) {
                    $salt                 = $adminModel::generateSalt();
                    $adminModel->password = $adminModel->pwdHash( $salt , $adminData[ 'password' ] );
                    $adminModel->salt     = $salt;
                }
            }

            $adminModel->save();
//            $adminModel->getLastSql();
            $this->save( $post );
            echo $this->getLastSql();
            return true;
        } else {
//            echo 'add';exit();
            //add
            if ( false === $adminModel->create($post) ) {
                return $adminModel->getError();
            }
            $adminModel->type_id = 2;
            $salt = $adminModel::generateSalt();
            $adminModel->password = $adminModel->pwdHash($salt,$_POST['password']);
            $adminModel->salt = $salt;
            $data['admin_id'] = $adminModel->add();

            //避免出错
            $data['name'] = $post['name'];
            $data['address'] = $post['address'];
            $data['phone'] = $post['phone'];
            $data['main_page'] = $post['main_page'];
            $data['mark'] = $post['mark'];
            $this->add($data);
            return true;
        }
    }

	/**
	 * 获得列表
	 *
	 * @return array
	 * @author
	 **/
	public function getList($firstRow='',$listRows='')
	{
		if(!$firstRow || !$listRows){
			$result = $this->limit($firstRow.','.$listRows)->select();
			$adminModel = D('Administrator');
			foreach ($result as $key => $value) {
				$result[$key] = array_merge($value,$adminModel->field('username,create_time,last_login_time')->where(array('id'=>$value['admin_id']))->find());
			}
			return $result;
		}else{
			return $this->count();
		}
	}

    /**
     * 关联删除
     * @param $id
     *
     * @return boolean
     */
    public function del( $id ) {
        $data       = $this->find( $id );
        $adminModel = D( 'Administrator' );
        $adminModel->where( array( 'id' => $data[ 'admin_id' ] , 'type_id' => 2 ) )->find();
        if ( $adminModel && $data ) {
            if ( $adminModel->delete( $data[ 'admin_id' ] ) ) {
                $this->delete( $id );
                return true;
            }else{
                return false;
            }

        } else {
            return false;
        }
    }
}//