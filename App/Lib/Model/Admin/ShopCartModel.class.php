<?php
// +----------------------------------------------------------------------
// | 购物车/订单详细
// +----------------------------------------------------------------------
// | Author: konakona
// | Date: {13-5-24}{上午10:32}
// | Version: $Id$
// +---------------------------------------------------------------------- 
class ShopCartModel extends Model {

    /**
     * 返回购物车详情
     * @param int $orderID
     * @param int $id
     *
     * @return array
     */
    public function getCartList( $orderID ) {
        $result = $this->where( array( 'order_id' => $orderID , 'type' => 1 ) )->select();
        if ( $result ) {
            foreach ( $result as $k => $v ) {
                $shop                    = D( 'Shop' )->where( array( 'id' => $v[ 'shop_id' ] ) )->find();
                $result[ $k ][ 'name' ]  = $shop[ 'name' ];
                $result[ $k ][ 'unit' ]  = $shop[ 'unit' ];
                $result[ $k ][ 'type' ]  = $shop[ 'type' ];
                $result[ $k ][ 'price' ] = $shop[ 'price' ];
                unset( $shop );
            }
        }
        return $result;
    }

    /**
     * 获得一笔购物车记录的总价
     * @param $data
     *
     * @return int
     */
    public function getTotalPrice( $data ) {
        $totalPrice = 0;
        foreach ( $data as $k => $v ) {
            $totalPrice += $v[ 'price' ] * $v[ 'num' ];
        }

        return $totalPrice;
    }
}
