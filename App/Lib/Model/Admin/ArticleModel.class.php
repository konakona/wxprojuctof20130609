<?php

class ArticleModel extends CommonModel{
	protected $_validate = array(
    	// array('title','require','名称未填或已经存在！',1,'unique'),
        array('title','require','名称未填！'),
    	array('content','require','内容未填'),
    );

	protected $_auto		=	array(
	    // array('status',1,self::MODEL_INSERT,'string'),
	    array('add_time','time',self::MODEL_INSERT,'function'),
    );
}