<?php
/**
 *
 * User: konakona http://www.crazyphper.com
 * Date: 13-5-19
 * Time: 下午5:53
 */
class OptionsModel extends CommonModel
{
	/**
	 * 获得所有的配置
	 * @return array
	 */
	public function getOptions(){
		$result = $this->field('id',true)->select();

		$list = array();
		foreach($result as $k=>$v){
			$list[$v['key']] = $v['value'];
		}
		return $list;
	}
}
