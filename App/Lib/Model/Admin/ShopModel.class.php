<?php
/**
 *
 * User: konakona http://www.crazyphper.com
 * Date: 13-5-18
 * Time: 下午10:37
 */
class ShopModel extends CommonModel
{
	protected $_validate  = array(
		array('name','require','请输入商品名称！',1,'',MODEL::MODEL_INSERT),
		array('name','','该商品已经存在！',1,'unique',1),
		array('cate_id','number','请选择商品所属分类！',1),
		array('price','require','请输入商品价格！',1),
//		array('img','require','请输入上传图片！',1),
		array('content','1,255','请输入1~255个字符的商品内容！',1,'length'),
		array('type','number','请选择商品属性！',1)
	);

	protected $_auto = array(
	array('add_time',NOW_TIME,MODEL::MODEL_INSERT),
);
}
