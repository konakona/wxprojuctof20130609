<?php
/**
 * 产品图片处理类
 * ==============================================
 * 版权所有 2013-2014 http://www.novaweb.cn
 * 这不是一个自由软件，未经授权，不允许任何个人或组织传播和使用。
 * ==============================================
 * @author: konakona
 * @Date: 13-1-23
 * @Time: 上午10:40
 * @Version :$Id$
 */

class ImageHandleByProject{
    
	public $finalSaveDir,$finalTempSaveDir,$completeTempSaveDir,$completeSaveDir,$saveCompleteFileName;
    
    public function __construct() {
        $this->completeTempSaveDir =$_SERVER['DOCUMENT_ROOT'].DS.$this->finalTempSaveDir = C('UPLOAD_IMAGE_TEMP_DIR').session_id().DS;
        $this->completeSaveDir = $_SERVER['DOCUMENT_ROOT'].DS.$this->finalSaveDir = C('UPLOAD_IMAGE_DIR').date('Y-m-d').DS;
        if(false === $this->createTodayDir()){
            return false;
        }else{
            return true;
        }
    }

	public function createTodayDir(){
        if(!file_exists($this->completeSaveDir)){
            if(false === mkdir($this->completeSaveDir,0755)) return false;
        }
        if(!file_exists($this->completeTempSaveDir)){
            if(false === mkdir($this->completeTempSaveDir,0755)) return false;
        }
    }
    
    /**
     *将临时文件保存到正式文件夹
     * 并将文件名以数组形式返回
     * @param type $id 
     * @return array
     */
    public function move($id){
        $arr = array();
        $dh = opendir($this->completeTempSaveDir);
        while (($file = readdir($dh)) !== false) {
            if($file == '..' || $file == '.') continue;
            copy($this->completeTempSaveDir.$file,$this->completeSaveDir.$file);
            if(substr($file,0,2) != 'm_'){
                $arr[] = $this->finalSaveDir.$file;
            }
        }
        return $arr;
    }
    
    /**
     * 上传在FILES中的文件到临时文件
     */
    public function upload(){
        import('@.ORG.Util.UploadFile');
        //导入上传类
        $upload = new UploadFile();
        //设置上传文件大小
        $upload->maxSize            = 3292200;
        //设置上传文件类型
        $upload->allowExts          = explode(',', 'jpg,gif,png,jpeg');
        //设置附件上传目录
//        $upload->savePath           = './Uploads/';
        $upload->savePath           = $this->completeTempSaveDir;
        //设置需要生成缩略图，仅对图像文件有效
        $upload->thumb              = true;
        // 设置引用图片类库包路径
        $upload->imageClassPath     = '@.ORG.Util.Image';
        //设置需要生成缩略图的文件后缀
        $upload->thumbPrefix        = 'm_';  //生产2张缩略图
        //设置缩略图最大宽度
        $upload->thumbMaxWidth      = '400';
        //设置缩略图最大高度
        $upload->thumbMaxHeight     = '400';
        //设置上传文件规则
        $upload->saveRule           = 'uniqid';
        //删除原图
        $upload->thumbRemoveOrigin  = false;
        if (!$upload->upload()) {
            //捕获上传异常
            return $upload->getErrorMsg();
        } else {
            //取得成功上传的文件信息
            $uploadList = $upload->getUploadFileInfo();
            echo '<img width="100" height="100" src="'.$this->finalTempSaveDir.$uploadList[0]['savename'].'"/>';
        }
    }
}