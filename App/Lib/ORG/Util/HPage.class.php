<?php
/**
 * 重写系统分页类
 * 主要是切换了一种样式
 *
 * @author konakona
 * @version $Id$
 **/
import("ORG.Util.Page");
class HPage extends Page{

	// 分页栏每页显示的页数
	public $rollPage = 50;

	protected $config  =    array('header'=>'条记录','prev'=>' << ','next'=>' >> ','first'=>'第一页','last'=>'最后一页','theme'=>'<div class="dataTables_paginate paging_bootstrap pagination"><ul> %upPage% %first%  %prePage% %linkPage% %nextPage% %downPage% %end%</ul></div>');


	/**
	 * 分页显示输出
	 * @access public
	 */
	public function show() {
		if(0 == $this->totalRows) return '';
		$p              =   $this->varPage;
		$nowCoolPage    =   ceil($this->nowPage/$this->rollPage);

		// 分析分页参数
		if($this->url){
			$depr       =   C('URL_PATHINFO_DEPR');
			$url        =   rtrim(U('/'.$this->url,'',false),$depr).$depr.'__PAGE__';
		}else{
			if($this->parameter && is_string($this->parameter)) {
				parse_str($this->parameter,$parameter);
			}elseif(is_array($this->parameter)){
				$parameter      =   $this->parameter;
			}elseif(empty($this->parameter)){
				unset($_GET[C('VAR_URL_PARAMS')]);
				$var =  !empty($_POST)?$_POST:$_GET;
				if(empty($var)) {
					$parameter  =   array();
				}else{
					$parameter  =   $var;
				}
			}
			$parameter[$p]  =   '__PAGE__';
			$url            =   U('',$parameter);
		}
		//上下翻页字符串
		$upRow          =   $this->nowPage-1;
		$downRow        =   $this->nowPage+1;
		if ($upRow>0){
			$upPage     =   "<li class ='prev'><a class='button grey' href='".str_replace('__PAGE__',$upRow,$url)."'>".$this->config['prev']."</a></li>";
		}else{
			$upPage     =   '';
		}

		if ($downRow <= $this->totalPages){
			$downPage   =   "<li class='next'><a class='button grey' href='".str_replace('__PAGE__',$downRow,$url)."'>".$this->config['next']."</a></li>";
		}else{
			$downPage   =   '';
		}
		// << < > >>
		if($nowCoolPage == 1){
			$theFirst   =   '';
			$prePage    =   '';
		}else{
			$preRow     =   $this->nowPage-$this->rollPage;
			$prePage    =   "<li><a class='button grey' href='".str_replace('__PAGE__',$preRow,$url)."' >...</a></li>";
			// $theFirst   =   "<li><a href='".str_replace('__PAGE__',1,$url)."' >".$this->config['first']."</a></li>";
		}
		if($nowCoolPage == $this->coolPages){
			$nextPage   =   '';
			$theEnd     =   '';
		}else{
			$nextRow    =   $this->nowPage+$this->rollPage;
			$theEndRow  =   $this->totalPages;
			$nextPage   =   "<li><a class='button grey' href='".str_replace('__PAGE__',$nextRow,$url)."' >...</a></li>";
			// $theEnd     =   "<li><a href='".str_replace('__PAGE__',$theEndRow,$url)."' >".$this->totalPages."</a></li>";
		}
		// 1 2 3 4 5
		$linkPage = "";
		for($i=1;$i<=$this->rollPage;$i++){
			$page       =   ($nowCoolPage-1)*$this->rollPage+$i;
			if($page!=$this->nowPage){
				if($page<=$this->totalPages){
					$linkPage .= "<li><a class='button grey' href='".str_replace('__PAGE__',$page,$url)."'>&nbsp;".$page."&nbsp;</a></li>";
				}else{
					break;
				}
			}else{
				if($this->totalPages != 1){
					$linkPage .= "<li class='active'><a >&nbsp;".$page."&nbsp;</a></li>";
				}
			}
		}
		$pageStr     =   str_replace(
			array('%header%','%nowPage%','%totalRow%','%totalPage%','%upPage%','%downPage%','%first%','%prePage%','%linkPage%','%nextPage%','%end%'),
			array($this->config['header'],$this->nowPage,$this->totalRows,$this->totalPages,$upPage,$downPage,$theFirst,$prePage,$linkPage,$nextPage,$theEnd),$this->config['theme']);
		return $pageStr;
	}

}