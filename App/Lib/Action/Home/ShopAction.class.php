<?php
/**
 * 产品
 *
 * @author konakona
 * @version $Id$
 **/
class ShopAction extends CommonAction
{

	/**
	 * 分类ID
	 * @var int
	 */
	protected $cateID = 0;
	protected $cateName = null;
	protected $typeID = 1;

	public function __construct()
	{
		parent::__construct();

		if (is_numeric($_GET['type']) && $_GET['type'] > 1)
		{
			$this->typeID = $_GET['type'];
		}

		if (isset($_GET['cate']) && is_numeric($_GET['cate']))
		{
			$this->cateID = $_GET['cate'];
		}

		//获得商品的分类名
		if ($this->cateID != 0) $this->cateName = $this->curModel->getCateName($this->cateID);
		$this->assign($this->cateID);
		$this->assign($this->cateName);
		$shopCart = D('ShopCart')->getMine($this->uid);
		$this->assign('shop_cart', $shopCart);
		$this->assign('total_price', D('ShopCart')->getTotalPrice($shopCart));
	}


	public function index()
	{
		$this->assign('cate_list', $this->curModel->getCate());
		$count = $this->curModel->getList($this->cateID, $this->typeID);
		$p = $this->_page($count, 15);
		$list = $this->curModel->getList($this->cateID, $this->typeID, $p->firstRow, $p->listRows);
		$this->assign('list', $list);
		$this->chooseTemplate();
	}

	/**
	 * 选择商品模板
	 */
	protected function chooseTemplate()
	{
		$tempName = null;
		switch ($this->typeID)
		{
			case '2': //秒杀
				$tempName = 'cx_index';
				$this->assign('title', '秒杀商品列表页');
				break;

			case '3': //促销、折扣
				$tempName = 'cx_index';
				$this->assign('title', '促销商品列表页');
				break;
			case '1':
			default :
				//普通商
				$tempName = 'index';
				$this->assign('title', '商品列表页');
				break;
		}
		$this->display($tempName);
	}



}