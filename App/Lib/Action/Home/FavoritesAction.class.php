<?php
/**
 * 我的最爱
 * User: konakona http://www.crazyphper.com
 * Date: 13-5-19
 * Time: 下午8:15
 */
class FavoritesAction extends CommonAction
{
	/**
	 * 新增最爱
	 * 如果已经有这个商品的记录，则不添加
	 */
	public function add()
	{
		$map = array('shop_id' => $this->id, 'uid' => $this->uid);
		if ($this->curModel->where($map)->count() == 0)
		{
			$this->curModel->add($map);
		}
		$this->success("ok");
	}

	public function index()
	{
		$this->assign('title', '我的最爱');
		$map = array('uid'=>$this->uid);
		//获得总数
		$count = $this->curModel->where($map)->count();
		$p = $this->_page($count, 15);
		//提取数据
		$myF = $this->curModel->where($map)->limit($p->firstRow . ',' . $p->listRows)->select();
		//获得商品信息
		if ($myF) foreach ($myF as $k => $v)
		{
			$myF[$k] = array_merge($myF[$k], D('Shop')->field('id,uid', true)->find($v['shop_id']));
		}

//		print_r($myF);

		$this->assign('list', $myF);
		$this->display();
	}

	/**
	 * 删除这个最爱
	 * @return string|void
	 */
	public function delete()
	{
		$shopID = $this->id;
		$this->curModel->where(array('shop_id' => $shopID))->delete();
		$this->success("ok");
	}

}
