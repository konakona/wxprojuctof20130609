<?php
// +----------------------------------------------------------------------
// | 接受威信客户端发送来的音频并邮寄至管理员处
// 参考文档：http://mp.weixin.qq.com/wiki/index.php?title=%E9%80%9A%E7%94%A8%E6%8E%A5%E5%8F%A3%E6%96%87%E6%A1%A3
// +----------------------------------------------------------------------
// | Author: konakona
// | Date: {13-5-25}{下午4:34}
// | Version: $Id$
// +---------------------------------------------------------------------- 
class WXAPISoundAction extends WXAPIAction {

    public function _empty() {
        $this->index();
    }

    /**
     * 将接受GET参数:signature、timestamp、nonce、echostr
     *
     */
    public function index() {
        if ( false === $this->getUserInfo() ) { //未注册，不能使用

        } else {
            $this->successWX();
        }
    }

    /**
     * 根据GET的signature威信加密签名，获得本站注册用户id
     * @return boolean|mxied
     */
    protected function getUserInfo() {
        $sign = $this->_get( 'signature' );
        $user = D( 'Member' )->where( array( 'wx_id' => $sign ) )->find();
        if ( !$user ) return false;
        return $user;
    }

    /**
     * 通过附件发送语音
     */
    protected function sendAttMail() {
        //时间戳timestamp
        $content = "威信用户：XX于".date($_GET['timestamp'],'Y-m-d H:i:s')."向您发送语音预定消息，\t\n
        请您试听附件中的语音及时为客户发送商品。";
    }
}
