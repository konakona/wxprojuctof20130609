<?php

class MemberAction extends CommonAction
{
	public function index()
	{
		$this->assign('hCredits', (int)$_SESSION['user']['credits'] - $this->_hCredits()); //可用积分
		$this->assign('title', '会员中心');
		$this->display();
	}

	/**
	 * 个人资料
	 */
	public function profile()
	{
		if ($_POST)
		{
			if(strlen($_POST['phone']) == 11 && is_numeric($_POST['phone'])){
				$data['id'] = $this->uid;
				$_SESSION['user']['phone'] = $data['phone'] = $_POST['phone'];
				$this->curModel->save($data);
				$this->success("修改成功！",U('Member/index'));
			}else{
				$this->error("请输入正确的手机号码！");
			}
		} else
		{
			$this->assign('title', '资料修改');
			$this->display();
		}
	}

	/**
	 * 交易记录
	 */
	public function purchase_history()
	{
        $model = D('Order');
        $map = array();
        $map[ 'boss_id' ] = $this->bossID;
		$map['uid'] = $this->uid;
        $count = $model->where($map)->count();
		$p = $this->_page($count,30);
        $list  = $model->where( $map )->limit( $p->firstRow . ',' . $p->listRows )->order( 'id desc' )->select();

        //获得相关商品信息
        if ( $list ) foreach ( $list as $k => $v ) {
			$list[ $k ][ 'shop_list' ] = D( 'Admin/ShopCart' )->getCartList( $v[ 'id' ] );
        }
        $this->assign( 'list' , $list );
        $this->assign( 'title' ,'交易记录');
        $this->display();
	}

	public function my_credits()
	{
		$this->assign('title', '积分兑换');
		$this->assign('hCredits', (int)$_SESSION['user']['credits'] - $this->_hCredits()); //可用积分
		$this->display();
	}

	public function use_credits()
	{
		if (_IS_POST)
		{

			if (!is_numeric($_POST['use_credits']))
			{
				$this->error("请选择要兑换的积分数量！");
			}

			//检测数据合法性
			$num = floor(($_SESSION['user']['credits'] / $this->options['credits_exchange']) / $this->options['credits_start_line']);
			for ($i = 1; $i <= $num; $i++)
			{
				$arr[] = $i * $this->options['credits_start_line'];
			}

			if (!in_array($_POST['use_credits'], $arr))
			{
				$this->error("请勿非法修改参数！");
			}


			$used_credits = $this->_hCredits();

			if ($_POST['use_credits'] + $used_credits > $_SESSION['user']['credits'])
			{
				$this->error("您的积分不足以兑换！");
			}


			$data['uid'] = $this->uid;
			$data['used_credits'] = $_POST['use_credits'];
			$data['money'] = $_POST['use_credits'] * $this->options['credits_exchange'];
			$data['add_time'] = time();

			$model = M('credits_used_list');
			$model->add($data);
//			echo $model->getLastSql();
			$this->success("您的兑换请求已发送！请等待管理员联系您！");

		}
	}

	/**
	 * 可用积分
	 */
	private function _hCredits()
	{
		$model = M('credits_used_list');
		$result = $model->where(array('uid' => $this->uid, 'used_credits', 'status' => 0))->select();

		foreach ($result as $k => $v)
		{
			/** @var $used_credits int */
			$used_credits += $v['used_credits'];
		}

		return $used_credits;
	}

	public function address_list()
	{
		$model = M('address');
		$this->assign('list', $model->where(array('uid' => $this->uid))->select());
		$this->assign('title', '地址管理');
		$this->display();
	}

	public function address_add()
	{
		$model = M('address');
		if ($_POST['address']!="")
		{
			$data['uid'] = $this->uid;
			$data['used'] = strval($_POST['used']);
			$data['address'] = $_POST['address'];
			$data['name'] = $_POST['name'];

			if($_POST['used'] == 1){
				if($model->where(array('uid'=>$this->uid,'used'=>1))->count() == 1){
//					$this->error("您已经有一个默认收货地址了",U('Member/address_list'));
					$this->error("您已经有一个默认收货地址了");
				}
			}
			if ($this->id)
			{
				//save
				$data['id'] = $this->id;
				$model->save($data);
//				echo $model->getLastSql();
			} else
			{
				//add
				$model->add($data);

			}
			$this->success("添加成功！",U('Member/address_list'));
		} else
		{
			if($this->id){
				$this->assign('one',$model->find($this->id));
			}
			$this->assign('title', '地址修改');
			$this->display();
		}
	}

	public function address_del()
	{
		$model = M('address');
		$this->assign('list', $model->where(array('uid' => $this->uid, 'id' => $this->id))->delete());
		$this->success("删除成功！");
	}
}