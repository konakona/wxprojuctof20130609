<?php
/**
 *
 * User: konakona http://www.crazyphper.com
 * Date: 13-6-7
 * Time: 下午2:15
 */
class TextOrderAction extends CommonAction
{

	public function _initialize()
	{
		parent::_initialize();
		$address = M('Address')->where(array('used' => 1, 'uid' => $this->uid))->find(); //用户的默认收货地址和姓名
		if (!$address)
		{
			$this->error("请先选择默认收货地址！", U('Member/address_list'));
		}
	}

	public function add()
	{

		if ($_POST['content'])
		{
			$this->_save();
		} else
		{
			$this->assign('title', '文字预订');
			$this->display();
		}
	}

	protected function _save()
	{
		$address = M('Address')->where(array('used' => 1, 'uid' => $this->uid))->find(); //用户的默认收货地址和姓名

		$data['add_time'] = time();
		$data['uid'] = $this->uid;
		$data['buy_way'] = 1;
		$data['text_content'] = $_POST['content'];
		$data['boss_id'] = $this->bossID;
		$data['user_address'] = $address['address'];
		$data['user_realname'] = $address['name'];
		$orderID = M('Order')->add($data);

		//清空本人的购物车
//		$this->curModel->where("id IN ({$id})")->setField(array('type' => 1, 'order_id' => $orderID));

		//		echo $this->curModel->getlastsql();

		//触发发送动作
		$this->sendOrderMail($orderID, $_POST['content']);
		$this->success("您已经预订成功！请等待发货！", U('Member/index'));
	}
}
