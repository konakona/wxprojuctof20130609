<?php
/**
 * 文章
 *
 * @author konakona
 * @version $Id$
 **/
class NewsAction extends CommonAction{

    public $cid = 0,$cname;

    public function index($cid=0){
        if($cid == 0 || !is_numeric($cid)){
            $this->error("非常抱歉！这篇文章不存在！");
        }

        $this->cid = $cid;

        //检查分类是否存在
        if($this->cname = M('ArticleCate')->getFieldById($cid,'name') == null){
            $this->error("您所访问的页面不存在，请重试","Index/index");
        }

        //根据分类选择模板
        switch ($this->cid) {
            case '':
                # code...
                break;
            
            default:
                # code...
                break;
        }
        $templateName = '';

        //获取分类下的文章
        $this->_getList();
        $this->assign('cname',$this->cname);
        $this->assign('cid',$this->cid);
        $this->display($templateName);
    }

    private function _getList(){
        $map = array();
        $map['cate_id'] = $this->cid;

        $model = D(ACTION_NAME);
        import('@.ORG.Util.NovaPage');
        $page = new NovaPage($model->where($map)->count(),C('PAGE_LIMIT')); 
        $this->assign('page',$page->show());
        $this->assign('list',$model->where($map)->limit($page->firstRow.','.$page->listRows)->select());
    }

    public function detail($id=0){
        if($id == 0 || !is_numeric($id)){
            $this->error("非常抱歉！这篇文章不存在！");
        }
        
        $one = $model->find($id)or die('嘻嘻～没有找到这篇文章');

        $this->cid = $one['cate_id'];

        //找到关联的分类
        if($this->cname = M('ArticleCate')->getFieldById($this->cid,'name') == null){
            $this->error("您所访问的页面不存在，请重试","Index/index");
        }

        $model = D(ACTION_NAME);
        $this->assign('one',$one);
        $this->assign('cname',$this->cname);
        $this->assign('cid',$this->cid);
        $this->display();
    }
}