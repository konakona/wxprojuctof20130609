<?php
/**
 * 内容（静态页）调用
 *
 * @author konakona
 * @version $Id$
 **/
class ContentAction extends CommonAction{
    public $templateName ; 

    public function _empty(){
        $this->templateName = ACTION_NAME;
        if(file_exists(TMPL_PATH.$this->getActionName().C('TMPL_FILE_DEPR').$this->templateName.C('TMPL_TEMPLATE_SUFFIX')))
            $this->display($this->templateName);
        else
            $this->error("Hello!这个页面不存在无法访问，可能您地址输入错误。");
    }
}