<?php
// +----------------------------------------------------------------------
// | 威信用控制器
// +----------------------------------------------------------------------
// | Author: konakona
// | Date: {13-5-25}{下午4:39}
// | Version: $Id$
// +----------------------------------------------------------------------

//define your token
define("TOKEN", "konakona");

class WXAPIAction extends Action {

	protected $bossID = 0;

	public function __construct(){
		parent::__construct();
		$this->bossID = (int)$_GET['id'];
		$this->_saveLog($_REQUEST);
		if(false === $this->checkSign()){
			exit('校验失败！');
		}else{
			$this->_saveLog('开始文字处理...\n\t');
			$this->responseMsg();
			$this->successWX();
		}
	}
	public function index(){}

	/**
	 * 保存所有的请求到文本日志中
	 * @param $data
	 */
	private function _saveLog($data){
		if(is_array($data)){
				$content = var_export($data,true);
		}else{
			$content = $data;
		}
		Log::write($content,Log::INFO,'',LOG_PATH.date('y_m_d').'_wx.log');
		Log::save();
	}

    /**
     * 威信同意成功接口
     */
    public function successWX() {
        die($_GET[ 'echostr' ]);
    }

    /**
     * 检查威信提交过来的签名是否正确
    加密/校验流程：
    1. 将token、timestamp、nonce三个参数进行字典序排序
    2. 将三个参数字符串拼接成一个字符串进行sha1加密
    3. 开发者获得加密后的字符串可与signature对比，标识该请求来源于微信
     *
     */
    public function checkSign() {
		$signature = $_GET["signature"];
		$timestamp = $_GET["timestamp"];
		$nonce = $_GET["nonce"];
		$tmpArr = array(TOKEN, $timestamp, $nonce);
		sort($tmpArr);
		$tmpStr = implode( $tmpArr );
		$tmpStr = sha1( $tmpStr );
		if( $tmpStr == $signature ){
			return true;
		}else{
			return false;
		}
	}

	public function responseMsg()
	{
		//get post data, May be due to the different environments
		$postStr = $GLOBALS["HTTP_RAW_POST_DATA"];
		//extract post data
		if (!empty($postStr)){

			//@todo debug
			$this->_saveLog($postStr);

			$postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
			$fromUsername = $postObj->FromUserName;
			$toUsername = $postObj->ToUserName;
			$keyword = trim($postObj->Content);
			$time = time();
			$textTpl = "<xml>
							<ToUserName><![CDATA[%s]]></ToUserName>
							<FromUserName><![CDATA[%s]]></FromUserName>
							<CreateTime>%s</CreateTime>
							<MsgType><![CDATA[%s]]></MsgType>
							<Content><![CDATA[%s]]></Content>
							<FuncFlag>0</FuncFlag>
							</xml>";
			if(!empty( $keyword ))
			{
				$this->_saveLog(_saveLog);

				if(strstr($keyword,'需要')){

					//根据用户的签名，获得用户在本站的注册uid，如果未注册，则提示先到网站注册才可发送预订
					$user = array();
					$user = D('Member')->where(array('wx_sign' => strval($fromUsername)))->find();
					if (!$user)
					{
						$msgType = "text";
						$contentStr = "您还没有绑定帐号！";
					} else
					{
						$msgType = "text";
						$contentStr = "感谢您的支持，预订成功！";
						$address                 = M( 'Address' )->where( array( 'used' => 1 , 'uid' => $user['uid'] ) )->find(); //用户的默认收货地址和姓名
						$data = array();
						$data['add_time'] = time();
						$data['status'] = 0;
						$data['buy_way'] = 2;
						$data['uid'] = $user['id'];
						$data[ 'user_address' ]  = $address[ 'address' ];
						$data[ 'user_realname' ] = $address[ 'name' ];
						$data['text_content'] = $keyword;
						$orderID = D('Order')->add($data);
						A('Common')->sendOrderMail($orderID,$keyword);
					}

				} elseif (strstr($keyword, '绑定'))
				{
					//检查格式
					list($str, $account, $pwd) = explode("+", $keyword);
					if ($account == "" || $pwd == "")
					{
						$msgType = "text";
						$contentStr = "输入不正确！";
					} else
					{
						$user = D('Member')->where(array('phone'=>$account,'password'=>md5($pwd)))->find();
						$data = array();
						$data['wx_sign'] = strval($fromUsername);
						$data['id'] = $user['id'];
						if (false === D('Member')->save($data))
						{
							$msgType = "text";
							$contentStr = "绑定失败！";
						} else
						{
							$msgType = "text";
							$contentStr = "绑定成功！";
						}
					}
				}else{
					$msgType = "text";
					$contentStr = "您好，如果您已经绑定了威信帐号，可以通过发送含有‘需要’字样的信息预订商品。格式参考：‘我需要预订两瓶牛奶和1包软红塔山，联系方式159xxxxxxx’！\n
					绑定帐号方法：\t
					输入“绑定+网站帐号+平台密码”  \t格式参考“绑定+159xxxxxx+mypassword” \n
					如果您还没有帐号，可以访问我们的网站http://".$_SERVER['HTTP_HOST']."/index.php/Index/index/bossID/".$this->bossID."选购商品和注册。";
				}
				$resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
				//返回我们的网站
				echo $resultStr;

			}else{
				echo "您好，请问您需要知道些什么呢？";
			}
		}
	}

	/**
	 * 消息回复
	 *
	 * 对于每一个POST请求，开发者在响应包中返回特定xml结构，对该消息进行响应（现支持回复文本、图文、语音、视频、音乐和对收到的消息进行星标操作）。

	微信服务器在五秒内收不到响应会断掉连接。
	 *
	 * 格式要求：
	 *  <xml>
	<ToUserName><![CDATA[toUser]]></ToUserName>
	<FromUserName><![CDATA[fromUser]]></FromUserName>
	<CreateTime>12345678</CreateTime>
	<MsgType><![CDATA[text]]></MsgType>
	<Content><![CDATA[content]]></Content>
	<FuncFlag>0</FuncFlag>
	</xml>
	 *
	 * ToUserName	 接收方帐号（收到的OpenID）
	FromUserName	 开发者微信号
	CreateTime	 消息创建时间
	MsgType	 text
	Content	 回复的消息内容,长度不超过2048字节
	FuncFlag	 位0x0001被标志时，星标刚收到的消息。
	 */
	public function re(){}
}
