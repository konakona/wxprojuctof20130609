<?php
/**
 * 用户注册功能
 */
class RegisterAction extends CommonAction
{

	public function index()
	{
		if ($this->uid > 0)
		{
			$this->error("系统已经捆绑了您的手机，无需再注册。");
		}
		if ($_POST)
		{
			$this->_createAccount();
		} else
		{
			$this->assign('title', '注册帐号');
			$this->display('Member:register');
		}
	}

	/**
	 * 创建帐号
	 */
	private function _createAccount()
	{
		$model = D('Member');
		if($model->where(array('phone'=>$this->_post('phone')))->count()>0){
			$this->error("该手机已注册过！");
		}
		if (false === $model->create())
		{
			$this->error($model->getError());
		} else
		{
			$id = $model->add();
			//存储默认地址
			$data['uid'] = $id;
			$data['used'] = 1;
			$data['address'] = $this->_post('address');
			$data['name'] = $this->_post('name');
			M('Address')->data($data)->add();
		}
	}

	public function logout(){
		session_destroy();
	}
}