<?php
/**
 * 首页
 *
 * @author konakona
 * @version $Id$
 **/
class IndexAction extends CommonAction
{
	public function index()
	{
		$this->assign('noreturn',true);
		if ($_SESSION['defaultMainpage'])
		{
			$this->display($_SESSION['defaultMainpage']);	//分店首页

		} else
		{
//			$this->sendOrderMail(7);
			$this->display('default'); //默认首页
		}
	}

}