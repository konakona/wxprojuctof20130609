<?php
/**
 *
 * User: konakona http://www.crazyphper.com
 * Date: 13-6-6
 * Time: 下午11:50
 */
class LoginAction extends CommonAction
{
	public function index()
	{
		if ($_POST)
		{
			$this->_login();
		} else
		{
			$this->assign('title','马上登录');
			$this->display('Member:login');
		}
	}

	protected function _login()
	{

		if($_POST['phone'] != "" && $_POST['password']!=""){
			$model = D('Member');
			$where = array();
			$where['password'] = md5($_POST['password']);
			$where['phone'] = $_POST['phone'];
			$where['boss_id'] = $this->bossID;
			$user = $model->where($where)->find();

			if($user){
				$model->where($where)->setField('last_login_time',time());
				$_SESSION['user'] = $user;
				$this->success("登录成功！正在回到首页...", U('Index/index'));
			}
		}

		$this->error("您输入的用户名或密码有误！");


	}
}
