<?php

class BossAction extends CommonAction {

    public $adminModel = null;

    /**
     * 分店列表页
     *
     * @return void
     * @author
     **/
    public function index() {
        $count = $this->curModel->getList();
        $p     = $this->_page( $count , 30 );
        $list  = $this->curModel->getList( $p->firstRow , $p->listRows );
        $this->assign('list',$list);
        $this->display();
    }

    /**
     *
     *
     * @return void
     * @author
     **/
    public function add() {
        if ( $_POST ) {
            $this->_save();
        } else {
            if ( $this->id ) {
                $one = $this->curModel->find( $this->id ) ;
                $one['username'] = D('Administrator')->where(array('id'=>$one['admin_id']))->getField('username');
                $this->assign( 'one' , $one);
            }
            $this->display();
        }
    }

    private function _save() {
        $message = $this->curModel->saveData( $_POST );
        if ( true !== $message ) {
            $this->error( $message );
        } else {
            $this->success( "成功添加！" , U( 'Boss/index' ) );
        }
    }

    public function delete() {
        $this->adminModel->del( $this->id );
    }

}