<?php
/**
 *
 * User: konakona http://www.crazyphper.com
 * Date: 13-5-22
 * Time: 下午6:14
 */
class OrderAction extends CommonAction {


    public function index() {

        //所有订单都被设置为已阅状态！
        $this->curModel->allHaveRead();
        $map = array();
        $this->bossID > 1 && $map[ 'boss_id' ] = $this->bossID;

        $count = $this->curModel->where( $map )->count();
        $p     = $this->_page( $count , 30 );
        $list  = $this->curModel->where( $map )->limit( $p->firstRow . ',' . $p->listRows )->order( 'id desc' )->select();

		$creditsRatio  = M( 'Options' )->where( array( 'key' => 'credits_ratio' ) )->getField( 'value' );
        //获得相关商品信息
        if ( $list ) foreach ( $list as $k => $v ) {
            $list[ $k ][ 'shop_list' ] = D( 'ShopCart' )->getCartList( $v[ 'id' ] );
			$list[$k]['total_price'] = D('ShopCart')->getTotalPrice($list[ $k ][ 'shop_list' ]);
			$list[$k]['get_credits'] = $list[$k]['total_price'] * $creditsRatio;
        }
//		print_r($list);
        $this->assign( 'list' , $list );
        $this->display();
    }

	/**
	 * 导出成excel
	 */
	public function toExcel()
	{
		$map = array();
		if ($this->bossID)
		{
			$map['boss_id'] = $this->bossID;
		}

//		if($_POST['start_date'] > 0){
//
//		}
//
//		if($_POST['end_date'] > 0){
//
//		}

		//字段对比
		$keys = array('id' => '订单编号', 'add_time' => '下单时间', 'status' => '订单状态', 'user_realname' => '收货名',
			'user_phone'=>'客户电话',
			'uid' => '用户ID', 'get_credits' => '获得积分', //积分
			'user_address' => '客户地址', 'boss_info' => '所属代理', //分店
			'delivery_time' => '预定送货日期','buy_way'=>'订购方式',
			'now_go'=>'是否立即送货',
			'shop_cate' => '购买货物的类型', 'shop_name' => '购买的货物名称','specifications'=>'货品规格',
			'unit'=>'货品单位','shop_price' => '货品单价', 'shop_total_price' => '商品总价','num'=>'购买数量');


		//先组织订单表数据
		$result = $this->curModel->field("read_type",true)->where($map)->select();
		foreach ($result as $k => $v)
		{
			//获得分店名
			if ($v['boss_id'] > 0)
			{
				$result[$k]['boss_info'] = M('BossInfo')->where(array('id' => $v['boss_id']))->getField('name');
			} else
			{
				$result[$k]['boss_info'] = '总店';
			}

			if($v['delivery_time'] == 0){
				$result[$k]['delivery_time'] = '无';
				$result[$k]['now_go'] = '是';
			}else{
				$result[$k]['delivery_time'] = date('Y-m-d H:i:s',$v['delivery_time']);
				$result[$k]['now_go'] = '否';
			}
			$result[$k]['add_time'] = date('Y-m-d H:i:s', $v['add_time']);
			$result[$k]['status'] = $v['status'] == 2 ? '失败' : '完成';
			$result[$k]['buy_way'] = $v['buy_way'] == 1 ? '客户端点选' : '威信文字预订';

		}

		$i = 0; //重组key

		$array = array();

		//再组织商品纤细数据
		foreach ($result as $k => $v)
		{
			//获得商品详细
			$cartList = D('ShopCart')->field('id,uid,cate_id,add_time,order_id,type',true)->where(array('order_id' => $v['id']))->select();
			if(!$cartList) continue;

			foreach ($cartList as $cart)
			{
				$shop = D('Shop')->field("id,img,content,type,num,sale_num,add_time,close_time", true)->where(array('id' => $cart['shop_id']))->find();
//				dump($cart);
//				dump($shop);

				$array[$i] = $v;
				$tmp = D('Member')->find($cart['uid']);
				$array[$i]['user_phone'] = $tmp['phone'];
				$array[$i]['get_credits'] = $shop['price'];
				$array[$i]['shop_name'] = $shop['name'];
				$array[$i]['shop_cate'] = D('ShopCate')->where(array('id' => $shop['cate_id']))->getField('name');
				unset($shop['cate_id']);
				unset($shop['name']);
				switch ($shop['type'])
				{
					case '0':
						$shop['type'] = '下架';
						break;
					case '1':
						$shop['type'] = '普通';
						break;
					case '2':
						$shop['type'] = '秒杀';
						break;
					case '3':
						$shop['type'] = '促销';
						break;
				}
				$array[$i]['shop_total_price'] = $cart['num'] * $shop['price'];
				$array[$i] = array_merge($array[$i], $shop,$result[$k]);
				$i++;
			}
		}

		//组合数据

		//改变key
		$data = array();
		foreach ($array as $kk => $v)
		{
			foreach ($v as $k => $i)
			{
				if ($keys[$k])
				{
					$data[$kk][$keys[$k]] = $i;
				}
			}
		}
//		print_r($data);
//		exit();
		$a = A('Admin/Excel');
		$a->setData($data);
		$a->export("销售记录");
		$a->buildFile();
//		$this->success();

	}

    /**
     * 改变订单状态
     */
    public function changeStatus( $type ) {
        $data  = array( 'status' => $type );
        $where = array( 'id' => $this ->id );
        $this->curModel->where( $where )->save( $data );
        $this->success( "操作成功!" );
    }


    /**
     * ajax请求是否有新的订单？
     */
    public function newOrderCount() {
        echo $this->curModel->where( array( 'read_type' => 0 ) )->count();
    }
}
