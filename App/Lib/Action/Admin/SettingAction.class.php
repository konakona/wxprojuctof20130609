<?php
/**
 *
 * User: konakona http://www.crazyphper.com
 * Date: 13-5-18
 * Time: 下午10:35
 */
class SettingAction extends CommonAction
{
	public function __construct()
	{
		parent::__construct();
		$this->curModel = D('Options');
	}

	public function index()
	{
		if ($_POST)
		{
			$this->_save();
		} else
		{
			$this->assign($this->curModel->getOptions());
			$this->display();
		}
	}

	private function _save()
	{
		foreach ($_POST as $k => $v)
		{
			if ($this->curModel->where(array('key' => $k))->count())
			{
				//save
				$this->curModel->where(array('key' => $k))->data(array('value' => $v))->save();
			} else
			{
				//add
				$this->curModel->data(array('key' => $k, 'value' => $v))->add();
			}
		}
		$this->success("成功！");
	}
}
