<?php
/**
 * 通用导入导出通用
 * 主要被用于调用
 * User: konakona http://www.crazyphper.com
 * Date: 13-5-28
 * Time: 下午5:41
 */

header("Content-type: text/html; charset=utf-8");
define('FILE_MAX_SIZE', '10240'); //文件最大允许10m
define('TMP_DIR', realpath('.') . '/tmp/'); //临时文件存放位置
define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');


class ExcelAction extends Action
{

	/**
	 * phpexcel 对象
	 * @var null
	 */
	protected $excel = null;

	/**
	 * 要导出的数组
	 * @var array
	 */
	protected $data = array();

	protected $write = null;

	protected $reader = null;

	public function __construct()
	{
		parent::__construct();
		//初始化excel
		import('@.ORG.COM.PHPExcel');
		$this->excel = new PHPExcel();
		/**默认用excel2007读取excel，若格式不对，则用之前的版本进行读取*/
		import('@.ORG.COM.PHPExcel.IOFactory');
		$this->reader = new PHPExcel_Reader_Excel2007();
	}

	/**
	 * 设置数据
	 * @param $data
	 */
	public function setData($data)
	{
		$this->data = $data;
	}

	/**
	 * 开始生成excel数据
	 *
	 * @param $title
	 */
	public function export($title)
	{
//		$this->excel->setActiveSheetIndex(0);
//		$objActSheet = $this->excel->getActiveSheet();

		// 创建文件格式写入对象实例, uncomment
		$this->writer = new PHPExcel_Writer_Excel5($this->excel); // 用于其他版本格式
		//or
//$this->writer = new PHPExcel_Writer_Excel2007($this->excel); // 用于 2007 格式
//$this->writer->setOffice2003Compatibility(true);

//设置文档基本属性
		$objProps = $this->excel->getProperties();
		$objProps->setCreator("konakona"); //相关作者
		$objProps->setLastModifiedBy("konakona");
		$objProps->setTitle("crazyphper build excel");
		$objProps->setKeywords("konakona");
		$objProps->setCategory($title);
		//设置当前的sheet索引，用于后续的内容操作。
		//一般只有在使用多个sheet的时候才需要显示调用。
		//缺省情况下，PHPExcel会自动创建第一个sheet被设置SheetIndex=0
//		$this->excel->setActiveSheetIndex(0);
		$objActSheet = $this->excel->getActiveSheet();

//设置当前活动sheet的名称
//		$objActSheet->setTitle($title);

//设置单元格内容
		/**取得最大的列号*/
		$allColumn = $this->getColumn();

		/**取得一共有多少行*/
		$allRow = $this->getRow();

        $i = "A";
        foreach ($this->data[0] as $k => $v)
        {
            $objActSheet->setCellValue($i++."1", $k);
            $objActSheet->getColumnDimension($i)->setWidth(20);
        }
        unset($i);

        $key = 0;
            /**从第二行开始输出，因为excel表中第一行为列名*/
            for ($currentNum = 2; $currentNum <= $allRow+1; $currentNum++)  //行
            { //横向循环
//                dump($this->data[$key]);
                for ($currentRow = 1; $currentRow <= $allColumn; $currentRow++) //列
                {
                    $currentColumn = "A";
                    foreach ($this->data[$key] as $k => $v)
                    {
//                        dump($currentColumn . $currentNum);
                        if($v==""){
                            $objActSheet->setCellValue($currentColumn . $currentNum, "无");
//                            $objActSheet->getColumnDimension($currentColumn)->setAutoSize(true);
                        }else{
                            $objActSheet->setCellValue($currentColumn . $currentNum, $v);
//                            $objActSheet->getColumnDimension($currentColumn)->setWidth(600);
                        }
                        $currentColumn++;
                    }
            }
                $key++;
        }


        $this->excel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
        $this->excel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(8);
        $this->excel->getActiveSheet()->getDefaultStyle()->getAlignment();
        $this->excel->getActiveSheet()->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


//		$objActSheet->setCellValue('A1', '字符串内容'); // 字符串内容
//		$objActSheet->setCellValue('A2', 26); // 数值
//		$objActSheet->setCellValue('A3', true); // 布尔值
//		$objActSheet->setCellValue('A4', '=SUM(A2:A2)'); // 公式

//显式指定内容类型
//		$objActSheet->setCellValueExplicit('A5', '8757584', PHPExcel_Cell_DataType::TYPE_STRING);

		//合并单元格
//		$objActSheet->mergeCells('B1:C22');

		//分离单元格
//		$objActSheet->unmergeCells('B1:C22');

		//设置宽度
//		$objActSheet->getColumnDimension('B')->setAutoSize(true);
//		$objActSheet->getColumnDimension('A')->setWidth(30);

//设置单元格内容的数字格式。
//如果使用了 PHPExcel_Writer_Excel5 来生成内容的话，
//这里需要注意，在 PHPExcel_Style_NumberFormat 类的 const 变量定义的
//各种自定义格式化方式中，其它类型都可以正常使用，但当setFormatCode
		//为 FORMAT_NUMBER 的时候，实际出来的效果被没有把格式设置为"0"。需要
		//修改 PHPExcel_Writer_Excel5_Format 类源代码中的 getXf($style) 方法，
//在 if ($this->_BIFF_version == 0x0500) { （第363行附近）前面增加一
		//行代码:
//if($ifmt === '0') $ifmt = 1;

//设置格式为PHPExcel_Style_NumberFormat::FORMAT_NUMBER，避免某些大数字
//被使用科学记数方式显示，配合下面的 setAutoSize 方法可以让每一行的内容
		//都按原始内容全部显示出来。
//		$objStyleA5 = $objActSheet->getStyle('A5');
//		$objStyleA5->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
//
////设置字体
//		$objFontA5 = $objStyleA5->getFont();
//		$objFontA5->setName('Courier New');
//		$objFontA5->setSize(10);
//		$objFontA5->setBold(true);
//		$objFontA5->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
//		$objFontA5->getColor()->setARGB('FFFF0000');
//		$objFontA5->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
//// $ objFontA5 ->getFont()->setColor(PHPExcel_Style_Color::COLOR_RED);
//
////设置对齐方式
//		$objAlignA5 = $objStyleA5->getAlignment();
//		$objAlignA5->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
//		$objAlignA5->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

//从指定的单元格复制样式信息.
//		$objActSheet->duplicateStyle($objStyleA5, 'B1:C22');


////默认列宽
//		$this->excel->getActiveSheet()->getDefaultColumnDimension()->setWidth(12);
////默认行宽
//		$this->excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(15);
    }

	/**
	 * 获取文件内容
	 * @return array
	 */
	protected function getFileContent()
	{
//		dump($this->excel);
		/**读取excel文件中的第一个工作表*/
		$currentSheet = $this->excel->getSheet(0);
		/**取得最大的列号*/
		$allColumn = $currentSheet->getHighestColumn();
		/**取得一共有多少行*/
		$allRow = $currentSheet->getHighestRow();
		$k = 0 ;
		/**从第二行开始输出，因为excel表中第一行为列名*/
		for ($currentRow = 2; $currentRow <= $allRow; $currentRow++)
		{
			/**从第A列开始输出*/
			for ($currentColumn = 'A'; $currentColumn <= $allColumn; $currentColumn++)
			{
				$val = $currentSheet->getCellByColumnAndRow(ord($currentColumn) - 65, $currentRow)->getValue();
				/**ord()将字符转为十进制数*/
				/**如果输出汉字有乱码，则需将输出内容用iconv函数进行编码转换，如下将gb2312编码转为utf-8编码输出*/
				// iconv('utf-8','gb2312', $val)
				$this->data[$k][] = $val;
			}
			$k ++ ;
		}
	}


	/**
	 * 创建文件
     * @return string
	 */
	public function buildFile()
	{
        $filename = 'download.xls';
        $this->writer->save($filename);
        if(isset($filename)){
        //文件的类型
        header('Content-type: application/force-download');
        //下载显示的名字
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        readfile("$filename");
        }
//
//
//		//在浏览器导出
//		header("Content-Type: application/force-download");
//		header('Content-Type: application/vnd.ms-excel');
//		header("Content-Disposition: attachment;filename=\"download.xls\"");
//		header('Cache-Control: max-age=0');
//		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
//		header("Pragma: no-cache");
	}

	/**
	 * 开始获取excel数据
	 * @param $file
	 * @return bool
	 */
	public function import($file)
	{
		if(!file_exists($file)){
			return "无法访问文件：".$file."，原因可能是上传失败或权限不足";
		}
		/**默认用excel2007读取excel，若格式不对，则用之前的版本进行读取*/
		if (!$this->reader->canRead($file))
		{
			$this->reader = new PHPExcel_Reader_Excel5();
			if (!$this->reader->canRead($file))
			{
				return false;
			}
		}

		$this->excel = $this->reader->load($file);

		$this->getFileContent();

		return $this->data;
	}

	/**取得最大的列号*/
	protected function getColumn()
	{
		return count($this->data[0]);
	}

	protected function getRow()
	{
		return count($this->data);
	}

}
