<?php
/**
 *
 * User: konakona http://www.crazyphper.com
 * Date: 13-5-18
 * Time: 下午9:58
 */
class ShopAction extends CommonAction
{

	/**
	 * 错误语句
	 * 可以配合import使用
	 * @var array
	 */
	protected $errorMessage = array();

	/**
	 * 临时保存的图片
	 * @var array
	 */
	protected $importImgList = array();
	/**
	 * 临时保存的数据
	 * @var array
	 */
	protected $importData = array();
	/**
	 * 临时保存的ID
	 * @var null
	 */
	protected $importIDs = null;


	public function __construct()
	{
		parent::__construct();
		$this->assign('cate_list', M('ShopCate')->select());
	}

//	public function index()
//	{
//		$this->display();
//	}

	/**
	 * 批量导出
	 */
//	public function exportExcel(){}

	public function _before_add()
	{
		if ($_POST)
		{
			$this->_save();
			exit();
		}
		if ($this->id)
		{
			$this->assign('one', $this->curModel->find($this->id));
		}
	}

	private function _save()
	{
		//如果为修改，检测是否修改了商品属性，如果为下架，则记录下架时间，其他不做修改
		if($this->id && $_POST['type'] == 0){
			$_POST['close_time'] = time();
		}

		if (false === $this->curModel->create())
		{
			$this->error($this->curModel->getError());
		}

		if ($_FILES['img']['size']>0)
		{
			$this->curModel->img = $this->normal_upload('img');
			if (!$this->curModel->img)
			{
				$this->error($this->curModel->img);
			}
			if($this->id){
				$file = realpath('.').$this->curModel->getField('img');
				file_exists($file) && unlink($file);
			}
		}
		$this->id ?$this->curModel->save() : $this->curModel->add();
		$this->success("操作成功！", U('Shop/index'));
	}

	/**
	 * 保存图片
	 *
	 * 可以被批量导入调用
	 * @param $file
	 * @return boolean
	 */
	protected function saveFiles($file)
	{
		import('@.ORG.Util.UploadFile');

		$flag = true;
		switch ($file['error'])
		{
			case 1:
				$this->errorMessage[] = "上传失败！文件超出服务器最大限制:" . ini_get('upload_max_filesize');
				$flag = false;
				break;
		}

		if (!$flag) return false;
		if (!$this->importIDs)
		{
//$this->importImgList
		} else
		{
//			 move_uploaded_file()$file['tmp_name']
		}
	}

	public function import()
	{
		if ($_FILES)
		{
			print_r($_FILES['excel']);
			$data = array();
			if($_FILES['excel']['error'] == 0){
				$array = A('Admin/Excel')->import($_FILES['excel']['tmp_name']);
				if($array==false){
					$this->error("格式有误！");
				}
				//赋予字段名称
					for($i = 0 ; $i < count($array) ; $i++){
						//分类
						$data[$i]['cate_name'] = $array[$i]['0'];
						//商品名
						$data[$i]['name'] = $array[$i]['1'];
						//图片
//						$data[$i]['cate_name'] = $array[$i]['0'];
						//内容
						$data[$i]['content'] = $array[$i]['3'];
						//规格
						$data[$i]['specifications'] = $array[$i]['4'];
						//单位
						$data[$i]['unit'] = $array[$i]['5'];
						//单价
						$data[$i]['price'] = $array[$i]['6'];
						//库存
						$data[$i]['num'] = $array[$i]['7'];
						//条码
						$data[$i]['code'] = $array[$i]['8'];

						//上架日期
						$data[$i]['add_time'] = strtotime($array[$i]['9']);
						//下架日期
						$data[$i]['close_time'] = strtotime($array[$i]['10']);
						//类型
						switch($array[$i]['type']){
							case '普通':$data[$i]['type'] = 1;break;
							case '促销':$data[$i]['type'] = 3;break;
							case '秒杀':$data[$i]['type'] = 2;break;
						}
					}
				foreach($data as $k=>$v){
					//获得分类id
					$v['cate_id'] = D('ShopCate')->where(array('name'=>$v['cate_name']))->getField('id');
					if(is_null($v['cate_id'])) continue;
					echo $this->curModel->data($v)->add();
				}
//				dump($data);
				$this->success("导入完毕！");
			}
		} else
		{
			$this->display();
		}
	}

	public function _before_delete()
	{
		$file = $this->curModel->where(array($this->curModel->getPK() => $this->id))->getField('img');

		if (file_exists(realpath('.') . $file) && $file)
		{
			echo unlink(realpath('.') . $file);
		}

	}
}
