<?php
/**
 *
 * User: konakona http://www.crazyphper.com
 * Date: 13-5-19
 * Time: 上午1:59
 */
class MemberAction extends CommonAction
{

	public function index()
	{
		//获取分店会员记录
		if ($this->bossID > 0)
		{
			$map['boss_id'] = $this->bossID;
		}
		if($_GET['wx_name']){
			$map['wx_name'] = $_GET['wx_name'];
			$this->assign('wx_name',$_GET['wx_name']);
		}
		$this->_list($this->curModel, $map);
		$this->display();
	}

	/**
	 * 将所有该分店的用户数据导出成excel
	 */
	public function toExcel()
	{
		if ($this->bossID)
		{
			$map['boss_id'] = $this->bossID;
		}
		//字段对比
		$keys = array('id' => '用户ID', 'username' => '威信昵称', 'phone' => '联系电话', 'add_time' => '注册时间',
			'last_login_time' => '最近登录时间', 'credits' => '当前积分', //积分
			'used_credits'=>'已使用积分',
			'level_num' => '', 'boss_info' => '所属代理', //分店
			'address' => '地址', //
			'realname' => '姓名', 'last_use_credits_time' => '最近使用积分时间', 'favorites' => '用户收藏', 'last_order_time' => '最后下单时间',);

        $result = $this->curModel->where($map)->select();

		foreach ($result as $k => $v)
			$result[$k]['add_time'] = date('Y-m-d H:i:s', $v['add_time']);
			//获得用户默认地址
			$address = M('Address')->where(array('uid' => $v['id'], 'used' => 1))->find();
			$result[$k]['address'] = $address['address'];
			$result[$k]['realname'] = $address['name'];
			$result[$k]['used_credits'] = (int)D('credits_used_list')->where(array('uid' => $v['id'], 'status' => 1))->Sum('used_credits');

			//获得用户收藏列表
			$fav = M('Favorites')->where(array('uid' => $v['id']))->select();
			foreach ($fav as $f)
			{
				$result[$k]['favorites'] .= M('Shop')->where(array('id' => $f['shop_id']))->getField('name') . '/';
			}

			//获得最后下单时间
			$result[$k]['last_order_time'] = date('Y-m-d H:i:s', M('Order')->where(array('uid' => $v['id'], 'boss_id' => $v['boss_id']))->getField('add_time'));

			//获得最后使用积分的时间
			$result[$k]['last_use_credits_time'] = date('Y-m-d H:i:s', M('credits_used_list')->where(array('uid' => $v['id'], 'status' => 1))->getField('add_time'));

			//获得分店名
			if ($v['boss_id'] > 0)
			{
				$result[$k]['boss_info'] = M('BossInfo')->where(array('id' => $v['boss_id']))->getField('name');
			} else
			{
				$result[$k]['boss_info'] = '总店';
			}


		$data = array();
		foreach ($result as $kk => $v)
		{
			foreach ($v as $k => $i)
			{
				if ($keys[$k])
				{
					$data[$kk][$keys[$k]] = $i;
				}
			}
		}
//print_r($data);
		$a = A('Admin/Excel');
		$a->setData($data);
		$a->export("用户信息");
		$a->buildFile();
//		$this->success();
	}

	public function level_setting()
	{
		if ($_POST)
		{
			if ($_POST['name'] && is_numeric($_POST['point']))
			{
				if (!$this->id)
				{
					M('Level')->add($_POST);
				} else
				{
					M('Level')->save($_POST);
				}
				$this->success("成功！");
			} else
			{
				$this->error("数据不完整，请重试");
			}
		} else
		{
			if ($this->id)
			{
				$this->assign('one', M('Level')->find($this->id));
			}
			$this->display();
		}
	}

	public function level_index()
	{
		$this->_list(M('Level'), array());
		$this->display();
	}

	public function level_delete()
	{
		M('Level')->delete($this->id);
		$this->success("成功");
	}

	/**
	 * 积分兑换历史记录
	 */
	public function credits_history()
	{
		$model = M('credits_used_list');
		$p = $this->_page($model->count(), 30);
		$list = $model->limit($p->firstRow . ',' . $p->listRows)->select();
		foreach ($list as $k => $v)
		{
			$user = array();
			$user = M('Member')->where('id=' . $v['uid'])->field('phone,username,credits')->find();
			$list[$k]['username'] = $user['username'];
			$list[$k]['phone'] = $user['phone'];
			$list[$k]['credits'] = $user['credits'];
			unset($user);
		}
		$this->assign('list', $list);
		$this->assign('page', $p->show());
		$this->display();
	}

	public function credits_status($type)
	{
		if (!in_array($type, array('0', '1', '2')))
		{
			$this->error("参数有误！");
		}

		$model = M('credits_used_list');
		$data = $model->find($this->id);

		if ($type == 1)
		{

			$used_credits = $data['used_credits'];
			$credits = M('Member')->where('id=' . $data['uid'])->getField('credits');

			if (($credits - $used_credits) < 0)
			{
				$this->error('该用户只有' . $credits . '积分，不足以兑换！');
			}
			M('Member')->where('id=' . $data['uid'])->setField('credits', $credits - $used_credits);
		}
		$model->where('id=' . $this->id)->setField('status', $type);
		$this->success("成功");
	}
}
