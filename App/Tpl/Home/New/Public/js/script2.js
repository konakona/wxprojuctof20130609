/******************************************************************
 File Name: Script.js Javascript functions for iBlogfolio
 Theme Name: iBlogfolio for iPhone
 Theme URI: http://adanarchila.com
 Description: Nice and clean dark theme for application developers or mobile sites.
 Author: Adan Archila
 Author URI: http://adanarchila.com
 ******************************************************************/
$(document).ready(function () {

    // First Level Dropdown Menu
    $('#nav').click(function () {
        $('#menu > ul').slideToggle();

        if ($(this).hasClass('expand-nav')) {
            $(this).removeClass('expand-nav');
            $(this).addClass("collapse-nav");
        } else {
            $(this).removeClass('collapse-nav');
            $(this).addClass("expand-nav");
        }

        return false;
    });

    // Second Level Dropdown Menu
    $('.dropdown a').click(function () {

        if ($(this).next('ul').is(":visible")) {
            $(this).next().next().remove('.arrow-dir');
            $(this).next('ul').slideToggle();
        } else {
            $(this).next('ul').slideToggle();
            $(this).parent().append('<span class="arrow-dir"></span>');
        }

        return false;
    });

    // toggle Open/Close Posts
    $('.open-close').click(function () {

        if ($(this).parent().siblings('.post-content').is(":visible")) {
            // $(this).removeClass('less').addClass('more');
            $(this).parent().parent().siblings('.post-content').toggleFade({ speedIn:"fast"});
        } else {
            // $(this).removeClass('more').addClass('less');
            $(this).parent().parent().siblings('.post-content').toggleFade({ speedIn:"fast"});
        }

        return false;
    });

    // toggle Open/Close Portfolio
    $('.open-close-portfolio').click(function () {
        if ($(this).parent().siblings('.portfolio-content').is(":visible")) {
            // $(this).removeClass('less').addClass('more');
            $(this).parent().parent().siblings('.portfolio-content').toggleFade({ speedIn:"fast"});
        } else {
            // $(this).removeClass('more').addClass('less');
            $(this).parent().parent().siblings('.portfolio-content').toggleFade({ speedIn:"fast"});
        }

        return false;
    });

    // toggleFade Function
    (function ($) {
        $.fn.toggleFade = function (settings) {
            settings = jQuery.extend(
                {
                    speedIn:"normal",
                    speedOut:settings.speedIn
                }, settings
            );
            return this.each(function () {
                var isHidden = jQuery(this).is(":hidden");
                jQuery(this)[ isHidden ? "fadeIn" : "fadeOut" ](isHidden ? settings.speedIn : settings.speedOut);
            });
        };
    })(jQuery);

    // FadeIn/FadeOut Search Box
    $('#searchbtn').click(function () {
        $(this).next().toggleFade({ speedIn:"fast"});
    });

});

// Slider 
// redefine Cycle's updateActivePagerLink function
// $.fn.cycle.updateActivePagerLink = function(pager, currSlideIndex) {
//     $(pager).find('li').removeClass('activeLI')
//         .filter('li:eq('+currSlideIndex+')').addClass('activeLI');
// };

$(function () {
    $('#slider').cycle({
        timeout:3000,
        pager:'#home-slideshow ul',
        next:'#next',
        prev:'#prev',
        pagerAnchorBuilder:function (idx, slide) {
            return '<li><a href="#" class="nav-dot"></a></li>';
        }
    });
});

$(function () {
    $('#portfolio-web').cycle({
        timeout:3000,
        pager:'#web-slideshow ul',
        next:'#next-web',
        prev:'#prev-web',
        pagerAnchorBuilder:function (idx, slide) {
            return '<li><a href="#" class="nav-dot"></a></li>';
        }
    });
});

$(function () {
    $('#portfolio-photo').cycle({
        timeout:3000,
        pager:'#photo-slideshow ul',
        next:'#next-photo',
        prev:'#prev-photo',
        pagerAnchorBuilder:function (idx, slide) {
            return '<li><a href="#" class="nav-dot"></a></li>';
        }
    });
});

$(function () {
    $('#portfolio-logo').cycle({
        timeout:3000,
        pager:'#logo-slideshow ul',
        next:'#next-logo',
        prev:'#prev-logo',
        pagerAnchorBuilder:function (idx, slide) {
            return '<li><a href="#" class="nav-dot"></a></li>';
        }
    });
});

$(function () {
    $('#portfolio-vector').cycle({
        timeout:3000,
        pager:'#vector-slideshow ul',
        next:'#next-vector',
        prev:'#prev-vector',
        pagerAnchorBuilder:function (idx, slide) {
            return '<li><a href="#" class="nav-dot"></a></li>';
        }
    });
});
