function getnums() {
    $.get(orderAPIURL + '/' + Math.random(), function(data){
        if(isNaN(data)){
            alert("服务器返回了异常请求！");
        }else{
            $('.badge-important').text(data);
            $('.order_num').text(data);
        }
    });
}
$(document).ready(function(){
    getnums();  //每次刷新页面都先执行一次，省得我再$this->assign()一次～嘿嘿！
    setInterval(getnums, 180000);  //3分钟
});